# -*- coding: UTF-8 -*- #
"""
@fileName:arucoParam.py
@author:Dextor
@time:2024-07-05
"""

import pickle
from cv2 import aruco

param_file_path = '../data/coordinate/aruco_params.pkl'  # 参数文件路径


# Save the parameters to the file
def save_parameters(file_path, parameters):
    with open(file_path, 'wb') as file:
        pickle.dump(parameters, file)


# Initialize and store the data
def initialize_and_save_parameters(file_path):
    parameters = {
        'adaptiveThreshWinSizeMin': 3,
        'adaptiveThreshWinSizeMax': 23,
        'adaptiveThreshWinSizeStep': 10,
        'minMarkerPerimeterRate': 0.03,
        'maxMarkerPerimeterRate': 4.0,
        'adaptiveThreshConstant': 7,
        'minCornerDistanceRate': 0.05,
        'minDistanceToBorder': 3,
        'minMarkerDistanceRate': 0.05,
        'cornerRefinementMethod': aruco.CORNER_REFINE_NONE,
        'cornerRefinementWinSize': 5,
        'cornerRefinementMaxIterations': 30,
        'cornerRefinementMinAccuracy': 0.1,
        'markerBorderBits': 1,
        'perspectiveRemovePixelPerCell': 4,
        'perspectiveRemoveIgnoredMarginPerCell': 0.13,
        'maxErroneousBitsInBorderRate': 0.35,
        'minOtsuStdDev': 5.0,
        'errorCorrectionRate': 0.6
    }

    save_parameters(file_path, parameters)
    print(f"Parameters have been saved to {file_path}")


# Example usage: Save the documents to the file
if __name__ == "__main__":
    initialize_and_save_parameters(param_file_path)
