import cv2
import zmq
import time

def bind_socket(socket, address):
    try:
        socket.bind(address)
        print(f"Bound to {address}")
        return True
    except zmq.ZMQError as e:
        print(f"Failed to bind {address}: {e}")
        return False

def camera_process():
    context = zmq.Context()

    socket1 = context.socket(zmq.PUB)
    socket2 = context.socket(zmq.PUB)

    bound1 = bind_socket(socket1, "tcp://*:5555")
    bound2 = bind_socket(socket2, "tcp://*:5556")

    if not bound1 and not bound2:
        print("Both ports are in use. Exiting camera process.")
        return

    cap = cv2.VideoCapture(0)

    while True:
        ret, frame = cap.read()
        if not ret:
            continue

        encode_param = [int(cv2.IMWRITE_JPEG_QUALITY), 90]
        _, buffer = cv2.imencode('.jpg', frame, encode_param)

        if bound1:
            socket1.send(buffer.tobytes(), zmq.NOBLOCK)
        if bound2:
            socket2.send(buffer.tobytes(), zmq.NOBLOCK)

        time.sleep(0)

    cap.release()
    if bound1:
        socket1.close()
    if bound2:
        socket2.close()
    context.term()
    print("Camera process stopped")


if __name__ == "__main__":
    camera_process()
