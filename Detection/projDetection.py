# -*- coding: UTF-8 -*- #
"""
@fileName:projDetection.py
@author:Dextor
@time:2024-06-05
"""

import cv2
import numpy as np
import os
import pickle

projector_image_paths = [f"../wall_image/projector_to_tag_frame_{i}.png" for i in range(1, 11)]
eye_image_paths = [f"../wall_image/eye_to_tag_frame_{i}.png" for i in range(1, 11)]
projection_area_file = "../data/coordinate/projection_area.pkl"

# Global variables for button states
button_state = {
    'ReDetect': False,
    'Finish': False
}


def on_mouse(event, x, y, flags, param):
    global button_state
    if event == cv2.EVENT_LBUTTONDOWN:
        if 10 <= x <= 110 and 10 <= y <= 60:  # Check if the click is within the "ReDetect" button area
            button_state['ReDetect'] = True
        elif 120 <= x <= 220 and 10 <= y <= 60:  # Check if the click is within the "Finish" button area
            button_state['Finish'] = True


# Load the image from file
def load_images_from_files(paths):
    images = []
    for path in paths:
        if not os.path.exists(path):
            print(f"Image path '{path}' does not exist.")
            continue
        image = cv2.imread(path)
        if image is None:
            print(f"Failed to load image from '{path}'.")
            continue
        images.append(image)
    return images


# Enhance the contrast of the image
def enhance_contrast(image):
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)  # Convert RGB to grayscale
    clahe = cv2.createCLAHE(clipLimit=2.0, tileGridSize=(8, 8))
    enhanced = clahe.apply(gray)  # Enhance local contrast
    return enhanced


# Apply Canny edge detection
def canny_edge_detection(image):
    edges = cv2.Canny(image, 50, 150)  # Adjust the thresholds as needed
    return edges


# Apply morphological operations to connect edges
def apply_morphological_operations(edges):
    kernel = np.ones((7, 7), np.uint8)
    dilated = cv2.dilate(edges, kernel, iterations=1)
    closed = cv2.morphologyEx(dilated, cv2.MORPH_CLOSE, kernel)
    return closed


# Filter small contours based on area and exclude dark regions
def filter_small_and_dark_contours(contours, min_area=500, image=None):
    filtered_contours = []
    for contour in contours:
        if cv2.contourArea(contour) > min_area:
            if image is not None:
                mask = np.zeros_like(image)
                cv2.drawContours(mask, [contour], -1, 255, thickness=cv2.FILLED)
                mean_val = cv2.mean(image, mask=mask)[0]
                if mean_val > 50:  # Exclude dark regions
                    filtered_contours.append(contour)
            else:
                filtered_contours.append(contour)
    return filtered_contours


# Contour detection and finding the largest quadrilateral
def find_largest_quadrilateral(edges, gray_image):
    contours, _ = cv2.findContours(edges, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    filtered_contours = filter_small_and_dark_contours(contours, min_area=500, image=gray_image)
    # Filter small and dark contours
    max_area = 0
    largest_quadrilateral = None
    for contour in filtered_contours:
        epsilon = 0.02 * cv2.arcLength(contour, True)
        approx = cv2.approxPolyDP(contour, epsilon, True)

        if len(approx) == 4:
            area = cv2.contourArea(approx)
            if area > max_area:
                max_area = area
                largest_quadrilateral = approx

    return largest_quadrilateral, max_area


# Function to label corners
def label_corners(corners):
    sorted_corners = sorted(corners, key=lambda x: (x[1], x[0]))
    top_left, top_right = sorted(sorted_corners[:2], key=lambda x: x[0])
    bottom_left, bottom_right = sorted(sorted_corners[2:], key=lambda x: x[0])
    return {
        "Top left corner": top_left,
        "Top right corner": top_right,
        "Bottom left corner": bottom_left,
        "Bottom right corner": bottom_right
    }


# Save data to a file
def save_data(filename, data):
    with open(filename, 'wb') as file:
        pickle.dump(data, file)
    print(f"Data saved to {filename}")


def remove_frame_files(prefix):
    if os.path.exists(projection_area_file):
        os.remove(projection_area_file)
        print(f"Removed {projection_area_file}")


# Main function
def detect_projection_area(image_paths, prefix):
    global button_state  # Add this line
    button_state = {  # This is the step to reset the value of the button state
        'ReDetect': False,
        'Finish': False
    }

    images = load_images_from_files(image_paths)
    if not images:
        remove_frame_files(prefix)  # Remove corresponding frame files
        return "REDETECT", None, None

    def perform_detection():
        overall_largest_quadrilateral = None
        overall_max_area = 0
        best_display_image = None
        best_edges = None

        for image in images:
            for _ in range(100):  # Detect for multiple times
                gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
                enhanced = enhance_contrast(image)
                blurred = cv2.GaussianBlur(enhanced, (5, 5), 0)
                edges = canny_edge_detection(blurred)
                morphed_edges = apply_morphological_operations(edges)
                quadrilateral, area = find_largest_quadrilateral(morphed_edges, enhanced)
                if quadrilateral is not None and area > overall_max_area:
                    overall_max_area = area
                    overall_largest_quadrilateral = quadrilateral
                    best_display_image = image.copy()
                    best_edges = morphed_edges

        return best_display_image, overall_largest_quadrilateral, best_edges, overall_max_area

    def save_and_cleanup(image, prefix):
        if image is not None:
            frame_files = [f"../wall_image/{prefix}_to_tag_frame_{i}.png" for i in range(1, 11)]
            for frame_file in frame_files:
                if os.path.exists(frame_file):
                    os.remove(frame_file)
                    print(f"Removed {frame_file}")
            clean_image = image.copy()
            cv2.imwrite(f"../wall_image/{prefix}_to_tag.png", clean_image)

    display_image, largest_quadrilateral, edges_image, overall_max_area = perform_detection()
    if display_image is not None and largest_quadrilateral is not None:
        cv2.namedWindow('Detected Projection Area')
        cv2.setMouseCallback('Detected Projection Area', on_mouse)

        while True:
            display_with_buttons = display_image.copy()
            # Draw the buttons
            cv2.rectangle(display_with_buttons, (10, 10), (110, 60), (0, 255, 0), -1)
            cv2.putText(display_with_buttons, 'ReDetect', (20, 40), cv2.FONT_HERSHEY_SIMPLEX, 0.6, (255, 255, 255), 2)
            cv2.rectangle(display_with_buttons, (120, 10), (220, 60), (0, 0, 255), -1)
            cv2.putText(display_with_buttons, 'Finish', (130, 40), cv2.FONT_HERSHEY_SIMPLEX, 0.6, (255, 255, 255), 2)

            # Draw the quadrilateral
            cv2.drawContours(display_with_buttons, [largest_quadrilateral], -1, (0, 255, 0), 2)

            cv2.imshow('Detected Projection Area', display_with_buttons)

            # Save the edge-detected image after each detection
            cv2.imwrite(f"../wall_image/{prefix}_detected_edges.png", edges_image)

            if button_state['ReDetect']:  # ReDetect
                remove_frame_files(prefix)
                cv2.destroyWindow('Detected Projection Area')
                return "REDETECT", None, None  # Add an additional return value

            if button_state['Finish']:  # Finish
                save_and_cleanup(display_image, prefix)
                cv2.destroyWindow('Detected Projection Area')  # Close the window when Finish is clicked
                break

            if cv2.waitKey(1) & 0xFF == ord('q'):
                break

        # Return the largest quadrilateral corners and area
        return "FINISH", label_corners(largest_quadrilateral.reshape(4, 2)), overall_max_area

    remove_frame_files(prefix)
    return "REDETECT", None, None


def run_detection():
    while True:
        status_projector, corners_projector, area_projector = detect_projection_area(projector_image_paths, "projector")
        if status_projector == "REDETECT":
            continue

        status_eye, corners_eye, area_eye = detect_projection_area(eye_image_paths, "eye")
        if status_eye == "REDETECT":
            continue

        if status_projector == "FINISH" and status_eye == "FINISH":
            if corners_projector is not None and corners_eye is not None:
                print("Projector to Tag Corners:")
                for corner_name, corner_value in corners_projector.items():
                    print(f"{corner_name}: {corner_value}")
                print("Largest Quadrilateral Area (Projector):", area_projector)

                print("\nEye to Tag Corners:")
                for corner_name, corner_value in corners_eye.items():
                    print(f"{corner_name}: {corner_value}")
                print("Largest Quadrilateral Area (Eye):", area_eye)

                # Save results only after both detections are finished
                save_data("../data/coordinate/projection_area_projector.pkl", {"corners": corners_projector, "area": area_projector})
                save_data("../data/coordinate/projection_area_eye.pkl", {"corners": corners_eye, "area": area_eye})

            break


if __name__ == "__main__":
    run_detection()
