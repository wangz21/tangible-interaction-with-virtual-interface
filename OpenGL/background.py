# -*- coding: UTF-8 -*- #
"""
@fileName: background.py
@author: Dextor
@time: 2024-07-15
"""

import sys
import os
import time as tm
import threading
import moderngl
import moderngl_window as mglw
import numpy as np
from queue import Queue
from pyrr import Matrix44, Quaternion, Vector3

sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
from Detection.tangibleDetection import detect_ar_tags, get_latest_rotation_angles  # 导入 detect_ar_tags 函数


class Camera:
    def __init__(self, fov_y, aspect_ratio, near, far):
        self.proj = Matrix44.perspective_projection(fov_y, aspect_ratio, near, far)
        self.view = Matrix44.look_at(
            eye=[0.0, 0.0, 5.0],
            target=[0.0, 0.0, 0.0],
            up=[0.0, 1.0, 0.0]
        )

    def get_projection_matrix(self):
        return self.proj

    def get_view_matrix(self):
        return self.view

    def set_view_matrix(self, view_matrix):
        self.view = view_matrix


class CameraAR(mglw.WindowConfig):
    gl_version = (3, 3)
    title = "CameraAR"
    resource_dir = os.path.normpath(os.path.join(__file__, '../../data'))
    resizable = True

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        self.start_time = tm.time()
        self.resolution_queue = Queue()
        self.aspect_ratio = self.wnd.aspect_ratio
        self.transformed_points = None
        self.resolution = None
        self.transformation_matrix = None
        self.resolution_lock = threading.Lock()
        self.transformation_lock = threading.Lock()
        self.stop_event = threading.Event()
        self.all_files_exist = False

        # Store resolutions
        self.first_screen_resolution = None
        self.following_resolutions = None

        # Provide default initial transformation matrix and projector to tag transforms
        self.initial_transformation_matrix = np.eye(4, dtype=np.float32)
        self.projector_to_tag_transforms = {}

        # Shader for rendering 3D objects
        self.prog3d = self.ctx.program(
            vertex_shader='''
                #version 330

                uniform mat4 Mvp;
                uniform mat4 View;

                in vec3 in_position;
                in vec3 in_normal;
                in vec2 in_texcoord_0;

                out vec3 v_vert;
                out vec3 v_norm;
                out vec2 v_text;

                void main() {
                    gl_Position = Mvp * View * vec4(in_position, 1.0);
                    v_vert = in_position;
                    v_norm = in_normal;
                    v_text = in_texcoord_0;
                }
            ''',
            fragment_shader='''
                #version 330

                uniform vec3 Color;
                uniform vec3 Light;
                uniform sampler2D Texture;

                in vec3 v_vert;
                in vec3 v_norm;
                in vec2 v_text;

                out vec4 f_color;

                void main() {
                    float lum = clamp(dot(normalize(Light - v_vert), normalize(v_norm)), 0.0, 1.0) * 0.8 + 0.2;
                    vec4 tex_color = texture(Texture, v_text);
                    f_color = vec4(Color * tex_color.rgb * lum, tex_color.a);
                }
            ''',
        )
        self.mvp = self.prog3d['Mvp']
        self.view = self.prog3d['View']
        self.light = self.prog3d['Light']
        self.color = self.prog3d['Color']
        self.texture = self.prog3d['Texture']

        # Load the 3D virtual object, also creating the VBO in this session
        self.scene_cube = self.load_scene('crate.obj')

        # Extract the VAO from the scene
        self.vao_cube = self.scene_cube.root_nodes[0].mesh.vao.instance(self.prog3d)
        self.texture = self.load_texture_2d('crate.png')

        # Define the initial position of the virtual object
        self.object_pos = np.array([0.0, 0.0, 0.0])

        # Flag to check if the OpenGL window is initialized
        self.opengl_initialized = False

        # Flag to control when to render the model
        self.render_model = False

        # Initialize the camera
        fov_y = 45.0
        near = 0.1
        far = 100.0
        self.camera = Camera(fov_y, self.aspect_ratio, near, far)

    def run_detection(self):
        if not self.all_files_exist:
            self.detect_white_area_once()

    def detect_white_area_once(self):
        print("Detecting white area...")
        self.required_files = [
            '../data/coordinate/ar_tag_position.pkl',
            '../data/coordinate/ar_tag_all_points.pkl',
            '../data/coordinate/projection_area.pkl',
            '../data/coordinate/process_and_overlay_tags.pkl',
            '../data/coordinate/new_tag_corners.pkl'
        ]
        self.all_files_exist = all(os.path.exists(file) for file in self.required_files)
        # Check required files
        print(self.all_files_exist)  # True
        if self.all_files_exist:
            self.render_model = True
            # Start the AR tag detection thread
            self.detection_thread = threading.Thread(target=detect_ar_tags, args=(30,))
            self.detection_thread.start()

    def resize(self, width, height):
        super().resize(width, height)
        self.aspect_ratio = width / height  # Update aspect ratio

    def render(self, current_time: float, frame_time: float):
        if not self.all_files_exist:
            self.run_detection()

        self.ctx.clear(1.0, 1.0, 1.0)

        if not self.render_model:
            return  # Skip rendering the model if detection is not completed

        self.ctx.enable(moderngl.DEPTH_TEST | moderngl.CULL_FACE)

        # Set viewport, maintain aspect ratio
        width, height = self.wnd.size
        aspect_ratio = self.aspect_ratio
        if width / height > aspect_ratio:
            viewport_width = int(height * aspect_ratio)
            viewport_height = height
            viewport_x = (width - viewport_width) // 2
            viewport_y = 0
        else:
            viewport_width = width
            viewport_height = int(width / aspect_ratio)
            viewport_x = 0
            viewport_y = (height - viewport_height) // 2

        self.ctx.viewport = (viewport_x, viewport_y, viewport_width, viewport_height)

        # First transformation: Apply the full transformation matrix to the model's MVP
        proj = self.camera.get_projection_matrix()

        scale = Matrix44.from_scale((1.0, 1.0, 1.0))

        # Apply initial translation for the object's position
        initial_translation = Matrix44.from_translation(self.object_pos)

        # Combine the transformations
        mvp = proj @ initial_translation @ scale

        # Get the latest rotation angles from tangibleDetection
        rotation_angles = get_latest_rotation_angles()
        if rotation_angles:
            rotation_x, rotation_y, rotation_z = np.radians(rotation_angles)

            # Create rotation matrices
            rotation_matrix_x = Matrix44.from_x_rotation(rotation_x)
            rotation_matrix_y = Matrix44.from_y_rotation(rotation_y)
            rotation_matrix_z = Matrix44.from_z_rotation(rotation_z)

            # Combine the rotation matrices
            rotation_matrix = rotation_matrix_x @ rotation_matrix_y @ rotation_matrix_z

            # Apply the rotation to the camera view
            initial_camera_view = Matrix44.look_at(
                eye=(0.0, 0.0, 5.0),
                target=(0.0, 0.0, 0.0),
                up=(0.0, 1.0, 0.0)
            )

            camera_view = rotation_matrix @ initial_camera_view
            self.camera.set_view_matrix(camera_view)
        else:
            # If no rotation data, use the default view matrix
            camera_view = self.camera.get_view_matrix()

        self.mvp.write(mvp.astype('f4'))
        self.view.write(camera_view.astype('f4'))  # 传递摄像机视图矩阵给着色器
        self.color.value = (1.0, 1.0, 1.0)
        self.light.value = (10.0, 10.0, 10.0)

        self.texture.use()
        self.vao_cube.render()


if __name__ == '__main__':
    CameraAR.run()
