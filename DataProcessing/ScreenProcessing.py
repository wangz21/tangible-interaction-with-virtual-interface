# -*- coding: UTF-8 -*- #
"""
@fileName:ScreenProcessing.py
@author:Dextor
@time:2024-07-15
"""
import threading
import cv2
import numpy as np
import mss
import time
import pickle
from DataProcessing.SceneTransform import process_and_display_transformation, apply_perspective_transform
from Detection.realTimeRvecTvec import detect_ar_tags_and_calculate_corners


def crop_image(image):
    # 找到图像中的非黑色区域
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    _, thresh = cv2.threshold(gray, 1, 255, cv2.THRESH_BINARY)
    contours, _ = cv2.findContours(thresh, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

    if contours:
        # 获取最大轮廓
        c = max(contours, key=cv2.contourArea)
        x, y, w, h = cv2.boundingRect(c)
        cropped_image = image[y:y + h, x:x + w]
        return cropped_image
    else:
        return image


def resize_image(image, target_width, target_height):
    # Resize the image to the target dimensions
    resized_image = cv2.resize(image, (target_width, target_height))
    return resized_image


def scale_points(src_points, dst_resolution, src_resolution, scale_factor):
    src_points = np.array(src_points, dtype='float32')
    scale_x = (dst_resolution[0] / src_resolution[0]) * scale_factor
    scale_y = (dst_resolution[1] / src_resolution[1]) * scale_factor
    src_points[:, 0] *= scale_x
    src_points[:, 1] *= scale_y
    return src_points


def order_points(pts):
    rect = np.zeros((4, 2), dtype="float32")

    s = pts.sum(axis=1)
    rect[0] = pts[np.argmin(s)]
    rect[2] = pts[np.argmax(s)]

    diff = np.diff(pts, axis=1)
    rect[1] = pts[np.argmin(diff)]
    rect[3] = pts[np.argmax(diff)]

    return rect


def center_points(points, img_width, img_height):
    center_x = np.mean(points[:, 0])
    center_y = np.mean(points[:, 1])

    offset_x = (img_width / 2) - center_x
    offset_y = (img_height / 2) - center_y

    centered_points = points + np.array([offset_x, offset_y])

    return centered_points


def get_perspective_transform_matrix(src_points, dst_resolution):
    src_points = np.array(src_points, dtype='float32')
    dst_points = np.array([
        [0, 0],
        [dst_resolution[0] - 1, 0],
        [dst_resolution[0] - 1, dst_resolution[1] - 1],
        [0, dst_resolution[1] - 1]
    ], dtype='float32')
    matrix = cv2.getPerspectiveTransform(src_points, dst_points)
    return matrix


def apply_custom_perspective_transform(image, src_points, proj_points, dst_resolution):
    src_points = np.array(src_points, dtype='float32')
    proj_points = np.array(proj_points, dtype='float32')
    if src_points.shape[0] != 4 or src_points.shape[1] != 2:
        raise ValueError(f"src_points shape is invalid: {src_points.shape}")
    dst_points = np.array([
        [0, 0],
        [dst_resolution[0] - 1, 0],
        [dst_resolution[0] - 1, dst_resolution[1] - 1],
        [0, dst_resolution[1] - 1]
    ], dtype='float32')
    matrix = cv2.getPerspectiveTransform(src_points, proj_points)
    transformed_image = cv2.warpPerspective(image, matrix, dst_resolution)
    return transformed_image


def get_bounding_box(points):
    x_coords = points[:, 0]
    y_coords = points[:, 1]
    return np.array([min(x_coords), min(y_coords), max(x_coords), max(y_coords)])


def is_bounding_box_inside(inner_box, outer_box):
    return (inner_box[0] >= outer_box[0] and inner_box[1] >= outer_box[1] and
            inner_box[2] <= outer_box[2] and inner_box[3] <= outer_box[3])


def adjust_scale_factor(ordered_pts_eye, transformed_corners, dst_resolution, transformed_width, transformed_height):
    scale_factor = 1.0
    max_iterations = 100
    for _ in range(max_iterations):
        scaled_pts_eye = scale_points(ordered_pts_eye, dst_resolution, (transformed_width, transformed_height), scale_factor)
        scaled_box = get_bounding_box(scaled_pts_eye)
        transformed_box = get_bounding_box(transformed_corners)
        if is_bounding_box_inside(transformed_box, scaled_box):
            break
        scale_factor += 0.05
    return scale_factor


class ScreenProcessor:
    def __init__(self, first_resolution, latest_resolution, display_scale=0.5):
        self.first_resolution = self.limit_resolution(first_resolution)
        self.latest_resolution = self.limit_resolution(latest_resolution)
        self.display_scale = display_scale
        self.proj_image_path = '../wall_image/projector_to_tag.png'
        self.new_corners_file = '../data/coordinate/new_tag_corners.pkl'
        self.pts_eye = None  # 添加一个属性来存储 pts_eye
        self.pts_proj = None

        self.stop_event = threading.Event()
        self.detect_thread = threading.Thread(target=self.detect_ar_tags)
        self.detect_thread.start()

    def limit_resolution(self, resolution):
        max_width, max_height = 1920, 1009
        width, height = resolution
        if width > max_width or height > max_height:
            return (min(width, max_width), min(height, max_height))
        return resolution

    def update_pts_eye(self, result):
        new_pts_eye = [
            result['top_top_left'],
            result['top_right'],
            result['bottom_left'],
            result['bottom_bottom_right']
        ]
        self.pts_eye = np.float32(new_pts_eye)

    def detect_ar_tags(self):
        detect_ar_tags_and_calculate_corners(callback=self.update_pts_eye, stop_event=self.stop_event)

    def capture_screen(self):
        screen_width, screen_height = self.latest_resolution
        page_width = 800

        with mss.mss() as sct:
            # 获取第二个屏幕的信息
            monitor = sct.monitors[2]

            # 使用 process_and_display_transformation 函数获取 M_proj_inv
            M_proj_inv, _ = process_and_display_transformation(self.proj_image_path, self.latest_resolution, page_width)

            while True:
                # 捕获屏幕
                screen = sct.grab(monitor)
                # 将图像数据转换为 NumPy 数组
                img = np.array(screen)

                # 如果分辨率与捕获到的图像不一致，进行缩放
                if img.shape[1] != screen_width or img.shape[0] != screen_height:
                    img = cv2.resize(img, (screen_width, screen_height))

                # 将图像从 BGRA 转换为 BGR
                img = cv2.cvtColor(img, cv2.COLOR_BGRA2BGR)

                # 原始图像中的四个角点
                original_corners = np.float32([
                    [0, 0],
                    [screen_width - 1, 0],
                    [screen_width - 1, screen_height - 1],
                    [0, screen_height - 1]
                ])

                # 应用透视变换
                transformed_img = apply_perspective_transform(img, M_proj_inv, (self.latest_resolution[0], self.latest_resolution[1]))

                # 获取变换后的图像分辨率
                transformed_height, transformed_width = transformed_img.shape[:2]

                # 变换后的四个角点
                transformed_corners = cv2.perspectiveTransform(np.array([original_corners]), M_proj_inv)[0]

                # 加载新的视角转换矩阵
                with open(self.new_corners_file, 'rb') as f:
                    combined_key_points = pickle.load(f)

                if combined_key_points is not None:
                    eye_points = combined_key_points['eye']
                    projector_points = combined_key_points['projector']
                else:
                    raise ValueError(f"Failed to load data from {self.new_corners_file}")

                if self.pts_eye is None:
                    self.pts_eye = np.float32([
                        eye_points['top_top_left'],
                        eye_points['top_right'],
                        eye_points['bottom_left'],
                        eye_points['bottom_bottom_right']
                    ])

                if self.pts_proj is None:
                    self.pts_proj = np.float32([
                        projector_points['top_top_left'],
                        projector_points['top_right'],
                        projector_points['bottom_left'],
                        projector_points['bottom_bottom_right']
                    ])

                # 按顺时针顺序排列点
                ordered_pts_eye = order_points(self.pts_eye)
                ordered_pts_proj = order_points(self.pts_proj)

                # 调整 scale_factor
                scale_factor_eye = adjust_scale_factor(ordered_pts_eye, transformed_corners, self.latest_resolution, transformed_width, transformed_height)
                scale_factor_proj = adjust_scale_factor(ordered_pts_eye, transformed_corners, self.latest_resolution,
                                                    transformed_width, transformed_height)

                # 将 eye 的四个点按比例放大
                scaled_pts_eye = scale_points(ordered_pts_eye, self.latest_resolution, (transformed_width, transformed_height), scale_factor_eye)
                scaled_pts_proj = scale_points(ordered_pts_proj, self.latest_resolution,
                                              (transformed_width, transformed_height), scale_factor_proj)

                # 将四个点移动到图像中心
                centered_pts_eye = center_points(scaled_pts_eye, screen_width, screen_height)
                centered_pts_proj = center_points(scaled_pts_proj, screen_width, screen_height)

                # 应用自定义的透视变换
                final_transformed_image = apply_custom_perspective_transform(transformed_img, centered_pts_eye, centered_pts_proj, self.latest_resolution)

                # 裁剪图像以去除黑色部分
                cropped_image = crop_image(final_transformed_image)

                # 拉伸裁剪后的图像以符合目标分辨率
                stretched_image = resize_image(cropped_image, self.first_resolution[0], self.first_resolution[1])

                # 缩小显示图像的大小
                display_width = int(self.first_resolution[0] * self.display_scale)
                display_height = int(self.first_resolution[1] * self.display_scale)
                resized_img = cv2.resize(stretched_image, (display_width, display_height))

                # 显示捕获的图像
                cv2.imshow('Captured Screen', resized_img)

                # 按 'q' 键退出
                if cv2.waitKey(1) & 0xFF == ord('q'):
                    break

                # 添加一个短暂的延迟以降低 CPU 占用率
                time.sleep(0.1)

        cv2.destroyAllWindows()

    def update_resolution(self, new_first_resolution, new_latest_resolution):
        self.first_resolution = self.limit_resolution(new_first_resolution)
        self.latest_resolution = self.limit_resolution(new_latest_resolution)


if __name__ == "__main__":
    # 设置分辨率
    first_resolution = (1920, 1009)
    latest_resolution = (1920, 1009)  # 根据实际需求设置分辨率

    processor = ScreenProcessor(first_resolution, latest_resolution, display_scale=1.0)
    processor.capture_screen()

    # 停止检测线程
    processor.stop_event.set()
    processor.detect_thread.join()
