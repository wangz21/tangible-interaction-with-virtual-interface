# -*- coding: UTF-8 -*- #
"""
@fileName: onceWhiteDetection.py
@author: Dextor
@time: 2024-07-15
"""

import numpy as np
import cv2
import mss


class OnceWhiteDetectionApp:
    def __init__(self):
        self.detected_resolution = None
        self.is_running = False
        self.detect_enabled = False

    def display_cv_window(self):
        cv2.namedWindow('First Screen Detection')
        cv2.setMouseCallback('First Screen Detection', self.on_mouse)

        with mss.mss() as sct:
            monitor = sct.monitors[1]  # Use the first monitor

            while True:
                screen = sct.grab(monitor)
                screen_np = np.array(screen)
                screen_bgr = cv2.cvtColor(screen_np, cv2.COLOR_BGRA2BGR)

                # Draw a button on the image
                cv2.rectangle(screen_bgr, (10, 10), (110, 60), (0, 255, 0), -1)
                cv2.putText(screen_bgr, "Detect", (20, 40), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 255, 255), 2)

                cv2.imshow('First Screen Detection', screen_bgr)

                if cv2.waitKey(1) & 0xFF == ord('q'):
                    self.on_close()
                    break

                if self.detected_resolution:
                    break

        cv2.destroyAllWindows()

    def on_mouse(self, event, x, y, flags, param):
        if event == cv2.EVENT_LBUTTONDOWN:
            # Check if the click is within the button
            if 10 <= x <= 110 and 10 <= y <= 60:
                self.detect_enabled = True
                self.detect_white_area()

    def detect_white_area(self):
        if self.is_running:
            return
        self.is_running = True

        with mss.mss() as sct:
            monitor = sct.monitors[1]  # Use the first monitor
            screen = sct.grab(monitor)
            screen_np = np.array(screen)

            screen_bgr = cv2.cvtColor(screen_np, cv2.COLOR_BGRA2BGR)
            gray = cv2.cvtColor(screen_bgr, cv2.COLOR_BGR2GRAY)

            _, thresh = cv2.threshold(gray, 200, 255, cv2.THRESH_BINARY)
            contours, _ = cv2.findContours(thresh, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

            if contours:
                largest_contour = max(contours, key=cv2.contourArea)
                x, y, w, h = cv2.boundingRect(largest_contour)

                if w < 1280 or h < 720:
                    w, h = 1280, 720

                resolution = (w, h)
                cv2.rectangle(screen_bgr, (x, y), (x + w, y + h), (0, 255, 0), 2)
            else:
                resolution = (1280, 720)

            self.detected_resolution = resolution
            print(f"First screen detected resolution: {resolution}")
            cv2.destroyAllWindows()  # Only destroy OpenCV windows, don't exit the program

    def get_detected_resolution(self):
        self.display_cv_window()
        return self.detected_resolution
