# -*- coding: UTF-8 -*- #
"""
@fileName:display_service.py
"""

import cv2
import zmq
import numpy as np


def display_camera_data():
    context = zmq.Context()
    socket = context.socket(zmq.SUB)
    socket.connect("tcp://localhost:5555")
    socket.setsockopt_string(zmq.SUBSCRIBE, '')

    while True:
        frame = socket.recv()
        npimg = np.frombuffer(frame, dtype=np.uint8)
        img = cv2.imdecode(npimg, 1)

        cv2.imshow('Camera Data', img)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

    cv2.destroyAllWindows()
    print("Display window closed")


if __name__ == "__main__":
    display_camera_data()
