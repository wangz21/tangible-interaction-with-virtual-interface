# -*- coding: UTF-8 -*-
"""
@fileName: tangibleDetection.py
@author: Dextor
@time: 2024-07-11
@Comment: Don't rotate too much, just for a brief show.
"""

import multiprocessing
import threading
import zmq
import os
import sys
import cv2
import numpy as np
import time
from cv2 import aruco
from scipy.spatial.transform import Rotation as R
from DataProcessing.cameraService import camera_process

# 手动添加模块搜索路径
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

from Detection.tagDetection import load_parameters, apply_parameters

# Global variable to store the latest rotation angles
latest_rotation_angles = None
latest_valid_rvec = None  # Global variable to store the latest valid rvec
latest_valid_tvec = None  # Global variable to store the latest valid tvec


def calculate_y_rotation(initial_quat, current_quat):
    # Initial_quat.inv represent the inverse rotation of the initial rotation value. And current_quat means the
    # current rotation, using this equation I can get the relative rotation from curren to initial
    relative_quat = initial_quat.inv() * current_quat

    # Extract the rotation angle around y-axis
    relative_rotvec = relative_quat.as_rotvec()

    # Get the angle in degrees, because I WANT the blue axis (Z axis) as the y value like inside OpenGL, so I did
    # transfer it to the opengl coordinate.
    y_angle = np.degrees(relative_rotvec[2])

    # Ensure angle is in range [0, 360]
    y_angle = (y_angle + 360) % 360

    return y_angle


# Same as the y rotation.
def calculate_x_rotation(initial_quat, current_quat):
    # Calculate the relative rotation
    relative_quat = initial_quat.inv() * current_quat

    # Extract the rotation angle around x-axis
    relative_rotvec = relative_quat.as_rotvec()

    # Get the angle in degrees
    x_angle = np.degrees(relative_rotvec[0])

    # Ensure angle is in range [0, 360]
    x_angle = (x_angle + 360) % 360

    return x_angle


# Same as the y rotation
def calculate_z_rotation(initial_quat, current_quat):
    # Calculate the relative rotation
    relative_quat = initial_quat.inv() * current_quat

    # Convert the relative quaternion to a rotation matrix
    rotation_matrix = relative_quat.as_matrix()

    # Extract the Euler angles from the rotation matrix
    euler_angles = R.from_matrix(rotation_matrix).as_euler('xyz', degrees=True)

    # Extract the rotation angle around z-axis (third component)
    z_angle = euler_angles[1]

    # Ensure angle is in range [0, 360]
    z_angle = (z_angle + 360) % 360

    return z_angle


def ensure_tag_facing_camera(rvec, tvec):
    global latest_valid_rvec, latest_valid_tvec

    # Convert the rotation vector to a rotation matrix
    rotation_matrix, _ = cv2.Rodrigues(rvec)

    # Get the z-axis vector of the rotation matrix
    z_axis = rotation_matrix[:, 2]

    # Check if the z-axis is pointing towards the camera
    if z_axis[2] > 0:  # If z-axis is pointing away from the camera
        # Use the latest valid data if available
        if latest_valid_rvec is not None and latest_valid_tvec is not None:
            return latest_valid_rvec, latest_valid_tvec
        else:
            # If no valid previous data, return None or handle accordingly
            return None, None
    else:
        # Update the latest valid rvec and tvec
        latest_valid_rvec = rvec
        latest_valid_tvec = tvec
        return rvec, tvec


def adjust_angle(angle):
    # Ensure angle is in range [0, 360]
    angle = (angle + 360) % 360
    return angle


def detect_ar_tags(print_interval=30):
    global latest_rotation_angles

    stop_event = threading.Event()

    context = zmq.Context()
    socket = context.socket(zmq.SUB)
    socket.connect("tcp://localhost:5555")
    socket.setsockopt_string(zmq.SUBSCRIBE, '')

    try:
        frame = socket.recv(zmq.NOBLOCK)
    except zmq.Again:
        print("Tangible: No image data detected, starting camera service...")
        camera_service_process = multiprocessing.Process(target=camera_process)
        camera_service_process.start()
        time.sleep(1)
        frame = socket.recv()

    aruco_dict = aruco.getPredefinedDictionary(aruco.DICT_ARUCO_ORIGINAL)
    param_file_path = '../data/coordinate/aruco_params.pkl'
    parameters = load_parameters(param_file_path)
    detector_params = apply_parameters(parameters)

    camera_matrix = np.array([[800, 0, 320],
                              [0, 800, 240],
                              [0, 0, 1]], dtype=np.float32)
    dist_coeffs = np.zeros((5, 1))

    initial_rotations = {}
    frame_count = 0

    collected_data = {}

    first_detected_tag_id = None

    while not stop_event.is_set():
        try:
            frame = socket.recv()
            npimg = np.frombuffer(frame, dtype=np.uint8)
            frame = cv2.imdecode(npimg, 1)
        except zmq.error.Again:
            print("Failed to read frame from ZeroMQ")
            break

        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        gray = cv2.GaussianBlur(gray, (5, 5), 0)

        corners, ids, rejected = aruco.detectMarkers(gray, aruco_dict, parameters=detector_params)

        if ids is not None:
            # Filter to only include tags with ID 200
            valid_indices = [i for i, id in enumerate(ids) if id[0] == 200]
            if len(valid_indices) == 0:
                continue

            corners = [corners[i] for i in valid_indices]
            ids = np.array([ids[i] for i in valid_indices])  # Convert ids to a NumPy array

            frame = aruco.drawDetectedMarkers(frame, corners, ids)

            marker_length = 0.1
            rvecs, tvecs, _ = aruco.estimatePoseSingleMarkers(corners, marker_length, camera_matrix, dist_coeffs)

            for i, id in enumerate(ids):
                rvec = rvecs[i]
                tvec = tvecs[i]
                id_int = id[0]

                # Track only the first detected tag ID
                if first_detected_tag_id is None:
                    first_detected_tag_id = id_int

                if id_int != first_detected_tag_id:
                    continue

                # Ensure the tag is facing the camera
                rvec, tvec = ensure_tag_facing_camera(rvec, tvec)

                # If the rvec or tvec is None, skip this frame
                if rvec is None or tvec is None:
                    continue

                rotation = R.from_rotvec(rvec.flatten())

                if id_int not in initial_rotations:
                    initial_rotations[id_int] = rotation

                # Collect data
                if id_int not in collected_data:
                    collected_data[id_int] = []

                collected_data[id_int].append(rotation)

        if frame_count % print_interval == 0 and frame_count != 0:
            for id_int in collected_data:
                if len(collected_data[id_int]) > 0:
                    initial_rotation = initial_rotations[id_int]

                    # Calculate average rotation
                    avg_rotation_vec = np.mean([rot.as_rotvec() for rot in collected_data[id_int]], axis=0)
                    current_rotation = R.from_rotvec(avg_rotation_vec)

                    # Rotation around x, y, z axes using quaternion
                    rotation_angle_x = calculate_x_rotation(initial_rotation, current_rotation)
                    rotation_angle_y = calculate_y_rotation(initial_rotation, current_rotation)
                    rotation_angle_z = calculate_z_rotation(initial_rotation, current_rotation)

                    # Print the rotation angles
                    print("Frame:", frame_count)
                    print(f"ID: {id_int}, Rotation Angle (X-axis): {rotation_angle_x:.2f} degrees")
                    print(f"ID: {id_int}, Rotation Angle (Y-axis): {rotation_angle_y:.2f} degrees")
                    print(f"ID: {id_int}, Rotation Angle (Z-axis): {rotation_angle_z:.2f} degrees")

                    # Update the latest rotation angles
                    latest_rotation_angles = (rotation_angle_x, rotation_angle_y, rotation_angle_z)

            # Clear collected data
            collected_data = {}

        frame_count += 1

        if cv2.waitKey(1) & 0xFF == ord('q'):
            stop_event.set()
            break

    socket.close()
    context.term()


def get_latest_rotation_angles():
    global latest_rotation_angles
    return latest_rotation_angles


if __name__ == "__main__":

    # Run the detect_ar_tags function in a separate thread
    detection_thread = threading.Thread(target=detect_ar_tags, args=(30,))
    detection_thread.start()

    try:
        while True:
            rotation_angles = get_latest_rotation_angles()
            if rotation_angles is not None:
                print(f"Latest Rotation Angles: X={rotation_angles[0]:.2f}, Y={rotation_angles[1]:.2f}, Z={rotation_angles[2]:.2f}")
            time.sleep(1)
    except KeyboardInterrupt:
        print("Exiting...")
