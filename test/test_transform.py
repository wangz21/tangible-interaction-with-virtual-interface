# -*- coding: UTF-8 -*- #
"""
@fileName: test_transform.py
@author: Dextor
@time: 2024-07-16
"""

import cv2
import numpy as np
import pickle
import os


def load_data(filename):
    if os.path.exists(filename):
        with open(filename, 'rb') as file:
            return pickle.load(file)
    print(f"Data file {filename} not found")
    return None


def main():
    new_corners_file = '../data/coordinate/new_tag_corners.pkl'
    projector_image_path = '../wall_image/projector_to_tag.png'

    # Load combined key points
    combined_key_points = load_data(new_corners_file)
    if combined_key_points is not None:
        print("Combined Key Points:")
        print(combined_key_points)
    else:
        print(f"Failed to load data from {new_corners_file}")
        return

    # Extract key points for projector and eye
    projector_points = combined_key_points['projector']
    eye_points = combined_key_points['eye']

    # Load the projector image
    projector_image = cv2.imread(projector_image_path)

    if projector_image is None:
        print(f"Error: Could not read the image from {projector_image_path}")
        return

    # Define points to be used for perspective transformation
    pts_src = np.float32([
        projector_points['top_top_left'],      # top-left
        projector_points['top_right'],         # top-right
        projector_points['bottom_left'],       # bottom-left
        projector_points['bottom_bottom_right']# bottom-right
    ])

    # Define destination points from the eye's key points
    pts_dst = np.float32([
        eye_points['top_top_left'],            # top-left
        eye_points['top_right'],               # top-right
        eye_points['bottom_left'],             # bottom-left
        eye_points['bottom_bottom_right']      # bottom-right
    ])

    # Calculate the perspective transform matrix
    matrix = cv2.getPerspectiveTransform(pts_src, pts_dst)

    # Apply the perspective transformation to the image
    transformed_image = cv2.warpPerspective(projector_image, matrix, (projector_image.shape[1], projector_image.shape[0]))

    # Display the transformed image
    cv2.imshow("Transformed Image", transformed_image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()


if __name__ == "__main__":
    main()
