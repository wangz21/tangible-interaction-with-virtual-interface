# -*- coding: UTF-8 -*- #
"""
@fileName:computeOrientation.py
@author:Dextor
@time:2024-06-09
"""

import cv2
import numpy as np


def compute_tag_orientation(captured_data, corners):
    orientations = {}

    for step, data in captured_data.items():
        rvecs_avg = data['rvecs_avg']
        tvecs_avg = data['tvecs_avg']

        for tag_id, rvec in rvecs_avg.items():
            tvec = tvecs_avg[tag_id]
            # 计算旋转矩阵 Calculate the rotation matrix
            R, _ = cv2.Rodrigues(rvec)

            # 将旋转矩阵转换为欧拉角
            sy = np.sqrt(R[0, 0] ** 2 + R[1, 0] ** 2)
            singular = sy < 1e-6

            if not singular:
                x = np.arctan2(R[2, 1], R[2, 2])
                y = np.arctan2(-R[2, 0], sy)
                z = np.arctan2(R[1, 0], R[0, 0])
            else:
                x = np.arctan2(-R[1, 2], R[1, 1])
                y = np.arctan2(-R[2, 0], sy)
                z = 0

            euler_angles = np.array([x, y, z])

            orientations[tag_id] = {
                'translation': tvec,
                'rotation_matrix': R,
                'euler_angles': euler_angles
            }

    # 将计算得到的角点位置添加到结果中
    if corners:
        orientations['top_right_corner'] = corners['top_right']
        orientations['bottom_left_corner'] = corners['bottom_left']
        orientations['top_top_left_corner'] = corners['top_top_left']
        orientations['bottom_bottom_right_corner'] = corners['bottom_bottom_right']

    return orientations
