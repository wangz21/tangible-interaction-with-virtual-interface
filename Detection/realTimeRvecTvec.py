# -*- coding: UTF-8 -*- #
"""
@fileName:realTimeRvecTvec.py
@author:Dextor
@time:2024-07-05
@Comment: 必须要两个 tag 同时有数据才能输出，一个有另一个没有则不理会，直接 continue
"""
import multiprocessing
import threading
import time
import cv2
import numpy as np
from cv2 import aruco
import zmq
import pickle
from collections import deque
from DataProcessing.cameraService import camera_process

param_file_path = '../data/coordinate/aruco_params.pkl'  # The path of the file


# Read the parameters from the document
def load_parameters(file_path):
    with open(file_path, 'rb') as file:
        parameters = pickle.load(file)
    return parameters


# Apply the parameter to the ArUco detector
def apply_parameters(parameters):
    params = aruco.DetectorParameters()
    params.adaptiveThreshWinSizeMin = parameters['adaptiveThreshWinSizeMin']
    params.adaptiveThreshWinSizeMax = parameters['adaptiveThreshWinSizeMax']
    params.adaptiveThreshWinSizeStep = parameters['adaptiveThreshWinSizeStep']
    params.minMarkerPerimeterRate = parameters['minMarkerPerimeterRate']
    params.maxMarkerPerimeterRate = parameters['maxMarkerPerimeterRate']
    params.adaptiveThreshConstant = parameters['adaptiveThreshConstant']
    params.minCornerDistanceRate = parameters['minCornerDistanceRate']
    params.minDistanceToBorder = parameters['minDistanceToBorder']
    params.minMarkerDistanceRate = parameters['minMarkerDistanceRate']
    params.cornerRefinementMethod = parameters['cornerRefinementMethod']
    params.cornerRefinementWinSize = parameters['cornerRefinementWinSize']
    params.cornerRefinementMaxIterations = parameters['cornerRefinementMaxIterations']
    params.cornerRefinementMinAccuracy = parameters['cornerRefinementMinAccuracy']
    params.markerBorderBits = parameters['markerBorderBits']
    params.perspectiveRemovePixelPerCell = parameters['perspectiveRemovePixelPerCell']
    params.perspectiveRemoveIgnoredMarginPerCell = parameters['perspectiveRemoveIgnoredMarginPerCell']
    params.maxErroneousBitsInBorderRate = parameters['maxErroneousBitsInBorderRate']
    params.minOtsuStdDev = parameters['minOtsuStdDev']
    params.errorCorrectionRate = parameters['errorCorrectionRate']
    return params


# Find the corners of the tag
def find_tag_corners(tag_corners):
    sorted_by_y = sorted(tag_corners, key=lambda c: c[1])
    top_points = sorted(sorted_by_y[:2], key=lambda c: c[0])
    bottom_points = sorted(sorted_by_y[2:], key=lambda c: c[0])
    top_left = top_points[0]
    top_right = top_points[1]
    bottom_left = bottom_points[0]
    bottom_right = bottom_points[1]
    return bottom_left, bottom_right, top_left, top_right


# Main function: Detect and calculate the corners of tags
def detect_ar_tags_and_calculate_corners(interval=5, callback=None, stop_event=None):
    aruco_dict = aruco.getPredefinedDictionary(aruco.DICT_ARUCO_ORIGINAL)
    parameters = load_parameters(param_file_path)  # Read the parameter from the file
    detector_params = apply_parameters(parameters)  # Apply parameter to the detector

    # Define camera matrix and distortion coefficients
    camera_matrix = np.array([[800, 0, 320],
                              [0, 800, 240],
                              [0, 0, 1]], dtype=np.float32)
    dist_coeffs = np.zeros((5, 1))

    context = zmq.Context()
    socket = context.socket(zmq.SUB)
    socket.connect("tcp://localhost:5556")
    socket.setsockopt_string(zmq.SUBSCRIBE, '')

    # 尝试接收数据，若失败则启动 camera_service
    try:
        frame = socket.recv(zmq.NOBLOCK)
    except zmq.Again:
        print("No image data detected, starting camera service...")
        camera_service_process = multiprocessing.Process(target=camera_process)
        camera_service_process.start()
        time.sleep(1)  # 等待服务启动
        frame = socket.recv()

    last_valid_data = None  # Store the last valid data when both tags are facing the camera

    # Deques to store the history of coordinates for smoothing
    history_length = 5
    bottom_left_history = deque(maxlen=history_length)
    top_right_history = deque(maxlen=history_length)
    top_top_left_history = deque(maxlen=history_length)
    bottom_bottom_right_history = deque(maxlen=history_length)

    # Frame data for averaging
    frame_data = []
    frame_count = 0

    def get_smoothed_coordinate(history, new_point):
        history.append(new_point)
        return np.mean(history, axis=0)

    def positions_changed(new_positions, last_positions):
        position_threshold = 10  # Threshold for position change to trigger an update
        for key in new_positions:
            if np.linalg.norm(new_positions[key] - last_positions[key]) > position_threshold:
                return True
        return False

    while not (stop_event and stop_event.is_set()):
        try:
            # Capture the frame from ZeroMQ
            frame = socket.recv()
            npimg = np.frombuffer(frame, dtype=np.uint8)
            frame = cv2.imdecode(npimg, 1)
        except zmq.error.Again:
            print("Failed to read frame from ZeroMQ")
            break

        # Preprocess the frame
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        gray = cv2.GaussianBlur(gray, (5, 5), 0)

        # Detect the ArUco tag
        corners, ids, rejected = aruco.detectMarkers(gray, aruco_dict, parameters=detector_params)

        if ids is not None:
            print(f"Detected ids: {ids.flatten()}")

        if ids is not None:
            # Draw axes for all detected tags
            marker_length = 0.1  # Set the length of the tag to 0.1m
            rvecs, tvecs, _ = aruco.estimatePoseSingleMarkers(corners, marker_length, camera_matrix, dist_coeffs)

            for i, rvec in enumerate(rvecs):
                tvec = tvecs[i]
                cv2.drawFrameAxes(frame, camera_matrix, dist_coeffs, rvec, tvec, marker_length)

            # Filter to only include tags with ID 60 and 80
            valid_indices = [i for i, id in enumerate(ids) if id[0] in [60, 80]]
            if len(valid_indices) == 0:
                # Update and show the frame before continuing
                cv2.imshow('Camera View', frame)
                if cv2.waitKey(1) & 0xFF == ord('q'):
                    break
                continue

            corners = [corners[i] for i in valid_indices]
            ids = np.array([ids[i] for i in valid_indices])  # Convert ids to a NumPy array

            frame = aruco.drawDetectedMarkers(frame, corners, ids)

            if len(valid_indices) == 2:  # Ensure exactly two tags are detected
                tag1_corners = corners[0][0]
                tag2_corners = corners[1][0]

                tag1_bottom_left, tag1_bottom_right, tag1_top_left, tag1_top_right = find_tag_corners(tag1_corners)
                tag2_bottom_left, tag2_bottom_right, tag2_top_left, tag2_top_right = find_tag_corners(tag2_corners)

                # Calculate average coordinates for both tags
                tag1_avg = np.mean(tag1_corners, axis=0)
                tag2_avg = np.mean(tag2_corners, axis=0)

                # Determine which tag is top-left (tag1) and which is bottom-right (tag2)
                if tag1_avg[0] < tag2_avg[0] and tag1_avg[1] < tag2_avg[1]:
                    top_tag_corners = tag1_corners
                    bottom_tag_corners = tag2_corners
                else:
                    top_tag_corners = tag2_corners
                    bottom_tag_corners = tag1_corners

                # Find corners again with updated tags
                top_bottom_left, top_bottom_right, top_top_left, top_top_right = find_tag_corners(top_tag_corners)
                bottom_bottom_left, bottom_bottom_right, bottom_top_left, bottom_top_right = find_tag_corners(
                    bottom_tag_corners)

                # Calculate the vector
                down_vector = top_bottom_left - top_top_left
                right_vector = top_top_right - top_top_left
                left_vector = bottom_bottom_left - bottom_bottom_right
                up_vector = bottom_top_right - bottom_bottom_right

                # Extend the vector until they intersect
                def find_intersection(p1, v1, p2, v2):
                    A = np.array([v1, -v2]).T
                    b = p2 - p1
                    try:
                        t, s = np.linalg.solve(A, b)
                        intersection = p1 + t * v1
                        return intersection
                    except np.linalg.LinAlgError:
                        print("Singular matrix, cannot find intersection.")
                        return None

                bottom_left = find_intersection(top_bottom_left, down_vector, bottom_bottom_left, left_vector)
                top_right = find_intersection(top_top_right, right_vector, bottom_top_right, up_vector)

                if bottom_left is None or top_right is None:
                    print("Could not calculate intersections due to singular matrix.")
                    continue

                # Collect frame data for averaging
                frame_data.append({
                    'bottom_left': bottom_left,
                    'top_right': top_right,
                    'top_top_left': top_top_left,
                    'bottom_bottom_right': bottom_bottom_right
                })

        frame_count += 1

        if frame_count >= interval:
            if frame_data:
                avg_data = {}
                for key in frame_data[0].keys():
                    avg_data[key] = np.mean([data[key] for data in frame_data], axis=0)

                last_valid_data = avg_data

                # Draw the position of the virtual AR Tag using the current valid data
                if last_valid_data:
                    cv2.circle(frame, tuple(last_valid_data['bottom_left'].astype(int)), 5, (255, 0, 0), -1)
                    cv2.circle(frame, tuple(last_valid_data['top_right'].astype(int)), 5, (255, 0, 0), -1)

                    # Draw the triangle
                    bottom_bottom_right_int = tuple(last_valid_data['bottom_bottom_right'].astype(int))
                    bottom_left_int = tuple(last_valid_data['bottom_left'].astype(int))
                    top_top_left_int = tuple(last_valid_data['top_top_left'].astype(int))
                    top_right_int = tuple(last_valid_data['top_right'].astype(int))

                    cv2.line(frame, bottom_bottom_right_int, bottom_left_int, (0, 255, 0), 2)
                    cv2.line(frame, bottom_left_int, top_top_left_int, (0, 255, 0), 2)
                    cv2.line(frame, top_top_left_int, top_right_int, (0, 255, 0), 2)
                    cv2.line(frame, top_right_int, bottom_bottom_right_int, (0, 255, 0), 2)

                    # Call the callback with the result
                    if callback:
                        callback(last_valid_data)

            frame_data = []
            frame_count = 0

        # Display the result frame
        cv2.imshow('Camera View', frame)

        # Press 'q' to exit early
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

    # Release the resources
    socket.close()
    context.term()
    cv2.destroyAllWindows()


if __name__ == "__main__":
    def print_callback(result):
        for tag, center in result.items():
            print(f'{tag}: {center}')
        print('===')


    stop_event = threading.Event()
    detect_ar_tags_and_calculate_corners(callback=print_callback, stop_event=stop_event)
