# -*- coding: UTF-8 -*- #
"""
@fileName:projector2eye.py
@author:Dextor
@time:2024-06-19
"""

import sys
import threading
import numpy as np
from scipy.spatial.transform import Rotation as R
import pickle
import os
import cv2

sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
from Detection.realTimeRvecTvec import detect_ar_tags_and_calculate_average


# Function to load data from a pickle file
def load_data(file_path):
    if os.path.exists(file_path):
        with open(file_path, 'rb') as file:
            data = pickle.load(file)
        return data
    return None


# Function to save data to a pickle file
def save_data(file_path, data):
    with open(file_path, 'wb') as file:
        pickle.dump(data, file)


# Function to calculate transformation matrix from rvec and tvec
def calculate_transformation_matrix(rvec, tvec):
    # Transfer the rotation vector to rotation matrix
    R_matrix, _ = cv2.Rodrigues(rvec)
    # Create a 4x4 matrix
    T = np.eye(4)
    # Put the rotation matrix to the top left (3x3)
    T[:3, :3] = R_matrix
    # Place the translation vector in the first three rows and fourth column of the transformation matrix
    T[:3, 3] = tvec.reshape((3,))
    return T


# Function to ensure the Z-axis is pointing outward
def ensure_z_axis_outward(rvec, tvec):
    R_matrix, _ = cv2.Rodrigues(rvec)
    z_axis = R_matrix[:, 2]
    if z_axis[2] < 0:  # If the Z-axis is not pointing outward, invert the rotation
        rvec = -rvec
        tvec = -tvec
    return rvec, tvec


# Function to compute the average transformation matrix for a given step and tag_id
def compute_average_transformation(captured_data, step, tag_id):
    rvec_avg = captured_data[step]['rvecs_avg'][tag_id]
    tvec_avg = captured_data[step]['tvecs_avg'][tag_id]
    return calculate_transformation_matrix(rvec_avg, tvec_avg)


# Convert rotation matrix to quaternion
def rotation_matrix_to_quaternion(matrix):
    rotation = R.from_matrix(matrix[:3, :3])
    return rotation.as_quat()


# Convert quaternion to rotation matrix
def quaternion_to_rotation_matrix(quaternion):
    rotation = R.from_quat(quaternion)
    return rotation.as_matrix()


# Function to average quaternions using eigen decomposition
def average_quaternions(quaternions):
    M = np.zeros((4, 4))
    for q in quaternions:
        # Calculate the outer product of quaternions (q * q^T)
        q = np.outer(q, q)
        # Add the outer accumulation to the matrix M
        M += q

    # Normalized covariance matrix, which is also get the average value of
    M /= len(quaternions)
    # Eigenvalue decomposition
    eigenvalues, eigenvectors = np.linalg.eigh(M)
    # Pick the biggest quaternions which can represent the average of quaternion the best
    avg_quaternion = eigenvectors[:, np.argmax(eigenvalues)]
    return avg_quaternion


# Main function to calculate projector to eye transformation
def calculate_projector_to_eye_transforms():
    # Load initial data
    ar_tag_position_file = '../data/coordinate/ar_tag_position.pkl'
    captured_data = load_data(ar_tag_position_file)

    # Ensure data is loaded
    if captured_data is None:
        raise ValueError("Captured data not found!")

    # Calculate initial transformation matrices from projector to tag and eye to tag
    projector_to_tag_transforms = {}
    eye_to_tag_transforms = {}

    for tag_id in captured_data['projector_to_tag']['rvecs_avg']:
        projector_to_tag_transforms[tag_id] = compute_average_transformation(captured_data, 'projector_to_tag', tag_id)

    for tag_id in captured_data['eye_to_tag']['rvecs_avg']:
        eye_to_tag_transforms[tag_id] = compute_average_transformation(captured_data, 'eye_to_tag', tag_id)

    # Initialize the transformation matrix from projector to eye
    initial_transforms = []
    for tag_id in projector_to_tag_transforms:
        if tag_id in eye_to_tag_transforms:
            T_projector_to_tag = projector_to_tag_transforms[tag_id]
            T_eye_to_tag = eye_to_tag_transforms[tag_id]

            # The transformation from the projector to the eye
            T_tag_to_eye = np.linalg.inv(T_eye_to_tag)
            T_projector_to_eye = np.dot(T_tag_to_eye, T_projector_to_tag)
            initial_transforms.append(T_projector_to_eye)

    # Extract initial quaternions and translations
    initial_quaternions = []
    initial_translations = []
    for T in initial_transforms:
        initial_quaternions.append(rotation_matrix_to_quaternion(T))
        initial_translations.append(T[:3, 3])

    initial_average_quaternion = average_quaternions(initial_quaternions)
    initial_average_translation = np.mean(initial_translations, axis=0)
    initial_average_quaternion /= np.linalg.norm(initial_average_quaternion)

    initial_rotation_matrix = quaternion_to_rotation_matrix(initial_average_quaternion)
    initial_transformation_matrix = np.eye(4)
    initial_transformation_matrix[:3, :3] = initial_rotation_matrix
    initial_transformation_matrix[:3, 3] = initial_average_translation

    return initial_transformation_matrix, projector_to_tag_transforms


def process_real_time_data(initial_transformation_matrix, projector_to_tag_transforms, callback, stop_event):
    def detect_callback(avg_results):
        real_time_transforms = []

        for tag_id in avg_results:
            if tag_id in projector_to_tag_transforms:
                rvec_avg, tvec_avg = avg_results[tag_id]
                if np.all(rvec_avg == 0) and np.all(tvec_avg == 0):
                    continue  # Skip default values

                # Ensure the Z-axis is pointing outward
                rvec_avg, tvec_avg = ensure_z_axis_outward(rvec_avg, tvec_avg)

                T_projector_to_tag = projector_to_tag_transforms[tag_id]
                T_eye_to_tag = calculate_transformation_matrix(rvec_avg, tvec_avg)
                T_tag_to_eye = np.linalg.inv(T_eye_to_tag)
                T_projector_to_eye = np.dot(T_tag_to_eye, T_projector_to_tag)
                real_time_transforms.append(T_projector_to_eye)

        if real_time_transforms:
            # Extract real-time quaternions and translations
            real_time_quaternions = []
            real_time_translations = []

            for T in real_time_transforms:
                real_time_quaternions.append(rotation_matrix_to_quaternion(T))
                real_time_translations.append(T[:3, 3])

            real_time_average_quaternion = average_quaternions(real_time_quaternions)
            real_time_average_translation = np.mean(real_time_translations, axis=0)
            real_time_average_quaternion /= np.linalg.norm(real_time_average_quaternion)

            real_time_rotation_matrix = quaternion_to_rotation_matrix(real_time_average_quaternion)
            real_time_transformation_matrix = np.eye(4)
            real_time_transformation_matrix[:3, :3] = real_time_rotation_matrix
            real_time_transformation_matrix[:3, 3] = real_time_average_translation

            callback(real_time_transformation_matrix)
        else:
            callback(initial_transformation_matrix)

    detect_ar_tags_and_calculate_average(callback=detect_callback, stop_event=stop_event)


if __name__ == "__main__":
    def print_callback(transformation_matrix):
        print("Transformation Matrix:")
        print(transformation_matrix)

    initial_transformation_matrix, projector_to_tag_transforms = calculate_projector_to_eye_transforms()
    stop_event = threading.Event()
    process_real_time_data(initial_transformation_matrix, projector_to_tag_transforms, callback=print_callback, stop_event=stop_event)
