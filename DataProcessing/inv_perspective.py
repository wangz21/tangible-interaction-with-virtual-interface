# -*- coding: UTF-8 -*- #
"""
@fileName:inv_perspective.py
@author:Dextor
@time:2024-06-09
"""

import cv2
import numpy as np
import pickle
import os
from Detection.tagDetection import detect_ar_tag_position
from Detection.quarDetection import process_ar_tags
from Detection.projDetection import detect_projection_area, projector_image_paths, eye_image_paths
from cv2 import aruco


# Save the data to particular position
def save_data(filename, data):
    with open(filename, 'wb') as file:
        pickle.dump(data, file)
    print(f"Data saved to {filename}")


# Load the data from particular position
def load_data(filename):
    if os.path.exists(filename):
        with open(filename, 'rb') as file:
            return pickle.load(file)
    print(f"Data file {filename} not found")
    return None


def overlay_image(bg_img, overlay_img, points):
    """
    Overlay the overlay_img on bg_img at the position specified by points.
    :param bg_img: The background image.
    :param overlay_img: The image to overlay.
    :param points: The points (4 corners) where the overlay_img should be placed.
    :return: The combined image.
    """
    h, w = overlay_img.shape[:2]
    overlay_points = np.float32([[0, 0], [w, 0], [w, h], [0, h]])

    # Calculate the perspective transform matrix and apply it to the overlay image
    matrix = cv2.getPerspectiveTransform(overlay_points, points)
    warped_overlay = cv2.warpPerspective(overlay_img, matrix, (bg_img.shape[1], bg_img.shape[0]))

    # Create a mask to determine the region to copy
    mask = np.zeros_like(bg_img, dtype=np.uint8)
    roi_corners = np.int32(points)
    cv2.fillConvexPoly(mask, roi_corners, (255, 255, 255))

    # Copy only the region defined by points from warped_overlay to the background image
    combined = cv2.bitwise_and(bg_img, cv2.bitwise_not(mask))
    combined = cv2.bitwise_or(combined, cv2.bitwise_and(warped_overlay, mask))

    return combined


def calculate_distance(point1, point2):
    return np.linalg.norm(np.array(point1) - np.array(point2))


def calculate_tag_size(corners):
    width = calculate_distance(corners[0], corners[1])
    height = calculate_distance(corners[0], corners[3])
    diagonal = np.sqrt(width ** 2 + height ** 2)
    return width, height, diagonal


# Calculate the scaled length
def adjust_points(points, scale_x, scale_y, tag_type):
    adjusted_points = points.copy()

    if tag_type == "40":
        vec_bottom_right = points[1] - points[0]
        vec_top_right = points[2] - points[1]
        vec_top_left = points[3] - points[0]

        adjusted_points[1] = adjusted_points[0] + vec_bottom_right * scale_x
        adjusted_points[2] = adjusted_points[1] + vec_top_right * scale_y
        adjusted_points[3] = adjusted_points[0] + vec_top_left * scale_y

    elif tag_type == "100":
        vec_bottom_right = points[1] - points[2]
        vec_bottom_left = points[0] - points[3]
        vec_top_left = points[3] - points[2]

        adjusted_points[1] = adjusted_points[2] + vec_bottom_right * scale_y
        adjusted_points[3] = adjusted_points[2] + vec_top_left * scale_x
        adjusted_points[0] = adjusted_points[3] + vec_bottom_left * scale_y

    return adjusted_points


def detect_tags_and_calculate_sizes(image_path):
    """
    Detect AR tags in the image and calculate their sizes.
    """
    img = cv2.imread(image_path)
    if img is None:
        print(f"Error: Could not read the image from {image_path}")
        return None

    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    # Load the ArUco dictionary and parameters
    aruco_dict = aruco.getPredefinedDictionary(aruco.DICT_ARUCO_ORIGINAL)
    parameters = aruco.DetectorParameters()

    # Detect the markers in the image
    corners, ids, _ = aruco.detectMarkers(gray, aruco_dict, parameters=parameters)

    if ids is None:
        print("No tags detected")
        return None

    avg_widths = []
    avg_heights = []

    for corner in corners:
        width, height, _ = calculate_tag_size(corner[0])
        avg_widths.append(width)
        avg_heights.append(height)

    avg_width = np.mean(avg_widths)
    avg_height = np.mean(avg_heights)

    return corners, avg_width, avg_height


def process_and_overlay_tags():
    print("Starting AR tag detection and orientation computation...\n")

    # File paths for storing data
    ar_tag_position_file = '../data/coordinate/ar_tag_position.pkl'
    ar_tag_all_points_file = '../data/coordinate/ar_tag_all_points.pkl'
    projection_area_file = '../data/coordinate/projection_area.pkl'
    process_and_overlay_tags_file = '../data/coordinate/process_and_overlay_tags.pkl'
    new_corners_file = '../data/coordinate/new_tag_corners.pkl'

    # If process_and_overlay_tags.pkl exist, then load and return the data
    if os.path.exists(process_and_overlay_tags_file):
        tag_positions = load_data(process_and_overlay_tags_file)
        if tag_positions:
            print(f"Loaded combined data from {process_and_overlay_tags_file}")
            return tag_positions

    while True:
        # Load data
        captured_data = load_data(ar_tag_position_file)
        all_points = load_data(ar_tag_all_points_file)

        # If data is not present, run detection
        if captured_data is None or all_points is None:
            captured_data, all_points = detect_ar_tag_position(camera_index=0, duration=5)

        # Check and return the projection area data
        projection_area_data = load_data(projection_area_file)
        if projection_area_data is None:
            status_proj, corners_proj, area_proj = detect_projection_area(projector_image_paths, "projector")
            if status_proj == "REDETECT":
                continue
            save_data(projection_area_file, (corners_proj, area_proj))
        else:
            corners_proj, area_proj = projection_area_data

        status_eye, corners_eye, area_eye = detect_projection_area(eye_image_paths, "eye")
        if status_eye == "REDETECT":
            captured_data = None
            all_points = None
            continue

        # Save data only if status is not REDETECT
        save_data(ar_tag_position_file, captured_data)
        save_data(ar_tag_all_points_file, all_points)

        break

    print("Captured Data:\n=============================")
    # rvec & tvec will be very useful in the projector 2 eye function.
    for step, data in captured_data.items():
        positions = data['average_positions']
        rvecs_avg = data['rvecs_avg']
        tvecs_avg = data['tvecs_avg']
        print(f"{step}:\n  Average Positions: {positions}\n  Average rvecs: {rvecs_avg}\n  Average tvecs: {tvecs_avg}")
        print("-----------------------------")

    tag_positions_combined = {}
    new_tag_positions_combined = {}

    # Process and overlay for both projector and eye images
    for device in ['projector', 'eye']:
        image_path = f'../wall_image/{device}_to_tag.png'
        output_path = f'../wall_image/tagged_image_{device}.png'
        bg_img = cv2.imread(image_path)

        if bg_img is None:
            print(f"Error: Could not read the background image from {image_path}")
            continue

        # Check and load the AR Tag size data
        # Ensure to recalculate the AR Tag sizes for each image
        tag_corners, avg_width, avg_height = detect_tags_and_calculate_sizes(image_path)
        if tag_corners is None:
            print("Error: No tags detected in the image.")
            continue

        if tag_corners is None:
            print("Error: No tags detected in the image.")
            continue

        # Ensure to reprocess the AR Tag corners for each image
        corners_detail, detailed_positions = process_ar_tags(image_path, None)

        if corners_detail:
            print(f"Top right corner: {corners_detail['top_right']}")
            print(f"Bottom left corner: {corners_detail['bottom_left']}")
            print(f"Top top left corner: {corners_detail['top_top_left']}")
            print(f"Bottom bottom right corner: {corners_detail['bottom_bottom_right']}")

            overlay_img_path_40 = '../data/Tag/40.png'
            overlay_img_40 = cv2.imread(overlay_img_path_40)

            if overlay_img_40 is None:
                print(f"Error: Could not read the overlay image from {overlay_img_path_40}")
                continue

            points_40 = np.float32([corners_detail['bottom_left'], corners_detail['bottom_bottom_right'], corners_detail['top_right'], corners_detail['top_top_left']])

            tag_width_40, tag_height_40, _ = calculate_tag_size(points_40)

            scale_x_40 = avg_width / tag_width_40
            scale_y_40 = avg_height / tag_height_40

            scaled_points_40 = adjust_points(points_40, scale_x_40, scale_y_40, "40")

            combined_img = overlay_image(bg_img, overlay_img_40, scaled_points_40)

            overlay_img_path_100 = '../data/Tag/100.png'
            overlay_img_100 = cv2.imread(overlay_img_path_100)

            if overlay_img_100 is None:
                print(f"Error: Could not read the overlay image from {overlay_img_path_100}")
                continue
            points_100 = np.float32([corners_detail['bottom_left'], corners_detail['bottom_bottom_right'], corners_detail['top_right'], corners_detail['top_top_left']])

            scaled_points_100 = adjust_points(points_100, scale_x_40, scale_y_40, "100")

            combined_img = overlay_image(combined_img, overlay_img_100, scaled_points_100)

            cv2.imwrite(output_path, combined_img)

            tag_positions = {
                "40": {
                    "position": scaled_points_40.tolist(),
                    "location": "bottom-left"
                },
                "100": {
                    "position": scaled_points_100.tolist(),
                    "location": "top-right"
                },
                "60": {
                    "position": [
                        detailed_positions["60"]["top_bottom_left"],
                        detailed_positions["60"]["top_bottom_right"],
                        detailed_positions["60"]["top_top_right"],
                        detailed_positions["60"]["top_top_left"]
                    ],
                    "location": "top-left"
                },
                "80": {
                    "position": [
                        detailed_positions["80"]["bottom_bottom_left"],
                        detailed_positions["80"]["bottom_bottom_right"],
                        detailed_positions["80"]["bottom_top_right"],
                        detailed_positions["80"]["bottom_top_left"]
                    ],
                    "location": "bottom-right"
                }
            }

            if device == "projector":
                save_data(process_and_overlay_tags_file, tag_positions)

            # Save the corner details to a new file
            new_tag_positions = corners_detail

            new_tag_positions_combined[device] = new_tag_positions

    save_data(new_corners_file, new_tag_positions_combined)

    return None


if __name__ == "__main__":
    process_and_overlay_tags()
