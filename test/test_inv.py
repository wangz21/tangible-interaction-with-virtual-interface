# -*- coding: UTF-8 -*- #
"""
@fileName:test_inv.py
@author:Dextor
@time:2024-07-15
"""
import cv2
import numpy as np
import pickle
import os
from DataProcessing.SceneTransform import process_and_display_transformation, apply_perspective_transform


def load_data(filename):
    if os.path.exists(filename):
        with open(filename, 'rb') as file:
            return pickle.load(file)
    print(f"Data file {filename} not found")
    return None


def crop_image(image):
    # 找到图像中的非黑色区域
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    _, thresh = cv2.threshold(gray, 1, 255, cv2.THRESH_BINARY)
    contours, _ = cv2.findContours(thresh, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

    if contours:
        # 获取最大轮廓
        c = max(contours, key=cv2.contourArea)
        x, y, w, h = cv2.boundingRect(c)
        cropped_image = image[y:y + h, x:x + w]
        return cropped_image
    else:
        return image


def resize_image(image, target_width, target_height):
    # Resize the image to the target dimensions
    resized_image = cv2.resize(image, (target_width, target_height))
    return resized_image


def scale_points(src_points, dst_resolution, src_resolution, scale_factor):
    src_points = np.array(src_points, dtype='float32')
    scale_x = (dst_resolution[0] / src_resolution[0]) * scale_factor
    scale_y = (dst_resolution[1] / src_resolution[1]) * scale_factor
    src_points[:, 0] *= scale_x
    src_points[:, 1] *= scale_y
    return src_points


def order_points(pts):
    rect = np.zeros((4, 2), dtype="float32")

    s = pts.sum(axis=1)
    rect[0] = pts[np.argmin(s)]
    rect[2] = pts[np.argmax(s)]

    diff = np.diff(pts, axis=1)
    rect[1] = pts[np.argmin(diff)]
    rect[3] = pts[np.argmax(diff)]

    return rect


def center_points(points, img_width, img_height):
    center_x = np.mean(points[:, 0])
    center_y = np.mean(points[:, 1])

    offset_x = (img_width / 2) - center_x
    offset_y = (img_height / 2) - center_y

    centered_points = points + np.array([offset_x, offset_y])

    return centered_points


def get_perspective_transform_matrix(src_points, dst_resolution):
    dst_points = np.array([
        [0, 0],
        [dst_resolution[0] - 1, 0],
        [dst_resolution[0] - 1, dst_resolution[1] - 1],
        [0, dst_resolution[1] - 1]
    ], dtype='float32')
    matrix = cv2.getPerspectiveTransform(src_points, dst_points)
    return matrix


def apply_custom_perspective_transform(image, src_points, dst_resolution):
    src_points = np.array(src_points, dtype='float32')
    dst_points = np.array([
        [0, 0],
        [dst_resolution[0] - 1, 0],
        [dst_resolution[0] - 1, dst_resolution[1] - 1],
        [0, dst_resolution[1] - 1]
    ], dtype='float32')
    matrix = cv2.getPerspectiveTransform(src_points, dst_points)
    transformed_image = cv2.warpPerspective(image, matrix, dst_resolution)
    return transformed_image


def get_bounding_box(points):
    x_coords = points[:, 0]
    y_coords = points[:, 1]
    return np.array([min(x_coords), min(y_coords), max(x_coords), max(y_coords)])


def is_bounding_box_inside(inner_box, outer_box):
    return (inner_box[0] >= outer_box[0] and inner_box[1] >= outer_box[1] and
            inner_box[2] <= outer_box[2] and inner_box[3] <= outer_box[3])


def adjust_scale_factor(ordered_pts_eye, transformed_corners, dst_resolution, transformed_width, transformed_height):
    scale_factor = 1.0
    max_iterations = 100
    for _ in range(max_iterations):
        scaled_pts_eye = scale_points(ordered_pts_eye, dst_resolution, (transformed_width, transformed_height), scale_factor)
        scaled_box = get_bounding_box(scaled_pts_eye)
        transformed_box = get_bounding_box(transformed_corners)
        if is_bounding_box_inside(transformed_box, scaled_box):
            break
        scale_factor += 0.05
    return scale_factor


def run_transformation(image_path):
    # 固定分辨率，放大内容
    dst_resolution = (1920, 1009)  # 使用与原始分辨率一致的值
    screen_width = 800  # 设置您的屏幕宽度

    # 投影图片的路径
    proj_image_path = '../wall_image/projector_to_tag.png'

    # 调用 process_and_display_transformation 函数以获取 M_proj_inv
    M_proj_inv, _ = process_and_display_transformation(proj_image_path, dst_resolution, screen_width)

    # 读取需要应用变换的图片
    image = cv2.imread(image_path)

    # 检查图像是否成功读取
    if image is None:
        raise ValueError(f"Failed to load image from {image_path}")

    # 获取图片的分辨率
    img_height, img_width = image.shape[:2]

    # 原始图像中的四个角点
    original_corners = np.float32([
        [0, 0],
        [img_width - 1, 0],
        [img_width - 1, img_height - 1],
        [0, img_height - 1]
    ])

    # 应用逆透视变换
    transformed_image = apply_perspective_transform(image, M_proj_inv, (img_width, img_height))

    # 获取变换后的图像分辨率
    transformed_height, transformed_width = transformed_image.shape[:2]

    # 变换后的四个角点
    transformed_corners = cv2.perspectiveTransform(np.array([original_corners]), M_proj_inv)[0]

    # 加载新的视角转换矩阵
    new_corners_file = '../data/coordinate/new_tag_corners.pkl'
    with open(new_corners_file, 'rb') as f:
        combined_key_points = pickle.load(f)

    if combined_key_points is not None:
        eye_points = combined_key_points['eye']
    else:
        raise ValueError(f"Failed to load data from {new_corners_file}")

    # 获取 eye 的四个点
    pts_eye = np.float32([
        eye_points['top_top_left'],
        eye_points['top_right'],
        eye_points['bottom_left'],
        eye_points['bottom_bottom_right']
    ])

    # 按顺时针顺序排列点
    ordered_pts_eye = order_points(pts_eye)

    # 调整 scale_factor
    scale_factor = adjust_scale_factor(ordered_pts_eye, transformed_corners, dst_resolution, transformed_width, transformed_height)

    # 将 eye 的四个点按比例放大
    scaled_pts_eye = scale_points(ordered_pts_eye, dst_resolution, (transformed_width, transformed_height), scale_factor)

    # 将四个点移动到图像中心
    centered_pts_eye = center_points(scaled_pts_eye, img_width, img_height)

    # 应用自定义的透视变换
    final_transformed_image = apply_custom_perspective_transform(transformed_image, centered_pts_eye, dst_resolution)

    # 裁剪图像以去除黑色部分
    cropped_final_image = crop_image(final_transformed_image)

    # 显示最终变换后的图像
    final_transformed_image_resized = resize_image(cropped_final_image, 1920, 1009)
    cv2.imshow("Final Transformed Image with Custom Perspective", final_transformed_image_resized)
    cv2.waitKey(0)
    cv2.destroyAllWindows()


if __name__ == "__main__":
    image_path = "../data/test_Img/1.png"
    run_transformation(image_path)
