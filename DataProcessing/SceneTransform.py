# -*- coding: UTF-8 -*- #
"""
@fileName:SceneTransform.py
@author:Dextor
@time:2024-06-17
"""

import cv2
import numpy as np
import pickle
import os


# Define a loading data function
def load_data(filename):
    if os.path.exists(filename):
        with open(filename, 'rb') as file:
            return pickle.load(file)
    print(f"Data file {filename} not found")
    return None


def get_perspective_transform_matrix(src_corners, dst_resolution, src_resolution):
    # Extract the corner coordinates from the dictionary
    src_pts = np.array([
        src_corners['Bottom right corner'],
        src_corners['Bottom left corner'],
        src_corners['Top left corner'],
        src_corners['Top right corner']
    ], dtype='float32')

    # Scale the source points based on the src_resolution
    src_pts[:, 0] *= dst_resolution[0] / src_resolution[0]
    src_pts[:, 1] *= dst_resolution[1] / src_resolution[1]

    # Define the destination points based on the given resolution
    dst_pts = np.array([
        [0, 0],  # Top-left corner
        [dst_resolution[0] - 1, 0],  # Top-right corner
        [dst_resolution[0] - 1, dst_resolution[1] - 1],  # Bottom-right corner
        [0, dst_resolution[1] - 1]  # Bottom-left corner
    ], dtype='float32')

    # Get the perspective transform matrix
    M = cv2.getPerspectiveTransform(src_pts, dst_pts)

    return M


# Apply the perspective transformation
def apply_perspective_transform(image, matrix, dst_resolution):
    return cv2.warpPerspective(image, matrix, dst_resolution)


def resize_image(image, target_width):
    # Resize the transformed images to fit the screen
    aspect_ratio = image.shape[1] / image.shape[0]
    target_height = int(target_width / aspect_ratio)
    resized_image = cv2.resize(image, (target_width, target_height))
    return resized_image


def process_and_display_transformation(proj_image_path, dst_resolution, screen_width):
    # Read the projection area data from data directory
    projection_area_file = '../data/coordinate/projection_area.pkl'
    projection_area_data = load_data(projection_area_file)
    if projection_area_data is None:
        raise ValueError("Failed to load projection area data")
    corners_proj, area = projection_area_data

    # Read the projection image to get its resolution
    proj_image = cv2.imread(proj_image_path)
    if proj_image is None:
        raise ValueError(f"Failed to load projection image from {proj_image_path}")
    src_resolution = (proj_image.shape[1], proj_image.shape[0])

    # Calculate the center of the four corners
    center_x = (corners_proj['Top left corner'][0] + corners_proj['Top right corner'][0] +
                corners_proj['Bottom right corner'][0] + corners_proj['Bottom left corner'][0]) / 4
    center_y = (corners_proj['Top left corner'][1] + corners_proj['Top right corner'][1] +
                corners_proj['Bottom right corner'][1] + corners_proj['Bottom left corner'][1]) / 4

    # Flip the corners based on the center
    def flip_corners(corners, center_x, center_y):
        flipped_corners = {}
        for key, corner in corners.items():
            flipped_corners[key] = [2 * center_x - corner[0], 2 * center_y - corner[1]]
        return flipped_corners

    # Flip the corners
    flipped_corners_proj = flip_corners(corners_proj, center_x, center_y)

    # Get the perspective transform matrix using flipped corners
    M_proj = get_perspective_transform_matrix(flipped_corners_proj, dst_resolution, src_resolution)

    M_proj_inv = np.linalg.inv(M_proj)

    # # Print the transformation matrix for projection area
    # print("Projection Area Perspective Transform Matrix:")
    # print(M_proj)

    # Apply the transformation to the projection image
    transformed_proj_image = apply_perspective_transform(proj_image, M_proj, dst_resolution)

    # Print the transformed corner coordinates for projection area
    # print("Transformed Projection Area Corners:")
    # for corner_name, corner_value in flipped_corners_proj.items():
    #     transformed_corner = cv2.perspectiveTransform(np.array([[corner_value]], dtype='float32'), M_proj)[0][0]
    #     print(f"{corner_name}: {np.round(transformed_corner).astype(int)}")

    # Load all the corners from the data directory
    process_and_overlay_tags_file = '../data/coordinate/process_and_overlay_tags.pkl'
    process_and_overlay_tags_data = load_data(process_and_overlay_tags_file)
    if process_and_overlay_tags_data is None:
        raise ValueError("Failed to load process_and_overlay_tags data")

    # Transform all points
    transformed_points = {}
    for tag_id, tag_data in process_and_overlay_tags_data.items():
        transformed_points[tag_id] = []
        for point in tag_data['position']:
            transformed_point = cv2.perspectiveTransform(np.array([[point]], dtype='float32'), M_proj)[0][0]
            transformed_points[tag_id].append(np.round(transformed_point).astype(int))

    # Draw all transformed points on the transformed image
    for tag_id, points in transformed_points.items():
        for point in points:
            cv2.circle(transformed_proj_image, tuple(point), 5, (0, 0, 255), -1)
            cv2.putText(transformed_proj_image, tag_id, tuple(point), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 1)

    return M_proj_inv, transformed_points


if __name__ == "__main__":
    # Fixed resolution for demonstration
    dst_resolution = (1920, 1009)
    screen_width = 800  # Set your desired width

    # Path to the projection image
    proj_image_path = '../wall_image/projector_to_tag.png'

    # Process and display the transformation
    M_proj_inv, transformed_points = process_and_display_transformation(proj_image_path, dst_resolution, screen_width)
