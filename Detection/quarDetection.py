# -*- coding: UTF-8 -*- #
"""
@fileName:quarDetection.py
@author:Dextor
@time:2024-06-07
Can Get the four corners of the quadrangle, no related to the other part, just get the data.
Correction: Need to use the part of projector -> tag instead of eye -> tag.
"""

import cv2
import numpy as np


def process_ar_tags(image_path, output_path=None):
    # Read the image
    image = cv2.imread(image_path)
    original_image = image.copy()  # Make a copy of the original image

    # Define the AR Tag detect function
    def detect_ar_tags(image):
        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        aruco_dict = cv2.aruco.getPredefinedDictionary(cv2.aruco.DICT_ARUCO_ORIGINAL)
        aruco_params = cv2.aruco.DetectorParameters()
        corners, ids, rejected = cv2.aruco.detectMarkers(gray, aruco_dict, parameters=aruco_params)
        return corners, ids

    # Detect the AR tag
    corners, ids = detect_ar_tags(image)

    # Check the system if there are two tags
    if corners is not None and len(corners) == 2:
        tag1_corners = corners[0][0]
        tag2_corners = corners[1][0]

        def find_tag_corners(tag_corners):
            sorted_by_y = sorted(tag_corners, key=lambda c: c[1])
            top_points = sorted(sorted_by_y[:2], key=lambda c: c[0])
            bottom_points = sorted(sorted_by_y[2:], key=lambda c: c[0])
            top_left = top_points[0]
            top_right = top_points[1]
            bottom_left = bottom_points[0]
            bottom_right = bottom_points[1]
            return bottom_left, bottom_right, top_left, top_right

        tag1_bottom_left, tag1_bottom_right, tag1_top_left, tag1_top_right = find_tag_corners(tag1_corners)
        tag2_bottom_left, tag2_bottom_right, tag2_top_left, tag2_top_right = find_tag_corners(tag2_corners)

        # Calculate average coordinates for both tags
        tag1_avg = np.mean(tag1_corners, axis=0)
        tag2_avg = np.mean(tag2_corners, axis=0)

        # Determine which tag is top-left (tag1) and which is bottom-right (tag2)
        if tag1_avg[0] < tag2_avg[0] and tag1_avg[1] < tag2_avg[1]:
            top_tag_corners = tag1_corners
            bottom_tag_corners = tag2_corners
        else:
            top_tag_corners = tag2_corners
            bottom_tag_corners = tag1_corners

        # Find corners again with updated tags
        top_bottom_left, top_bottom_right, top_top_left, top_top_right = find_tag_corners(top_tag_corners)
        bottom_bottom_left, bottom_bottom_right, bottom_top_left, bottom_top_right = find_tag_corners(bottom_tag_corners)

        # Calculate the vector
        down_vector = top_bottom_left - top_top_left
        right_vector = top_top_right - top_top_left
        left_vector = bottom_bottom_left - bottom_bottom_right
        up_vector = bottom_top_right - bottom_bottom_right

        # Extend the vector until they intersect
        def find_intersection(p1, v1, p2, v2):
            A = np.array([v1, -v2]).T
            b = p2 - p1
            try:
                t, s = np.linalg.solve(A, b)
                intersection = p1 + t * v1
                return intersection
            except np.linalg.LinAlgError:
                print("Singular matrix, cannot find intersection.")
                return None

        # The vector calculation in the correct direction
        bottom_left = find_intersection(top_bottom_left, down_vector, bottom_bottom_left, left_vector)
        top_right = find_intersection(top_top_right, right_vector, bottom_top_right, up_vector)

        if bottom_left is None or top_right is None:
            print("Could not calculate intersections due to singular matrix.")
            return None, None

        # Make sure the result is type int
        bottom_bottom_right_int = tuple(bottom_bottom_right.astype(int))
        bottom_left_int = tuple(bottom_left.astype(int))
        top_top_left_int = tuple(top_top_left.astype(int))
        top_right_int = tuple(top_right.astype(int))

        # Draw the position of the virtual AR Tag only if output_path is specified
        if output_path:
            cv2.circle(image, tuple(bottom_left.astype(int)), 5, (0, 0, 255), -1)
            cv2.circle(image, tuple(top_right.astype(int)), 5, (0, 0, 255), -1)

            # Draw the triangle
            cv2.line(image, bottom_bottom_right_int, bottom_left_int, (0, 255, 0), 2)
            cv2.line(image, bottom_left_int, top_top_left_int, (0, 255, 0), 2)
            cv2.line(image, top_top_left_int, top_right_int, (0, 255, 0), 2)
            cv2.line(image, top_right_int, bottom_bottom_right_int, (0, 255, 0), 2)

            cv2.imwrite(output_path, image)

        # Create tag positions dictionary
        detailed_positions = {
            "60": {
                "top_bottom_left": top_bottom_left.tolist(),
                "top_bottom_right": top_bottom_right.tolist(),
                "top_top_left": top_top_left.tolist(),
                "top_top_right": top_top_right.tolist()
            },
            "80": {
                "bottom_bottom_left": bottom_bottom_left.tolist(),
                "bottom_bottom_right": bottom_bottom_right.tolist(),
                "bottom_top_left": bottom_top_left.tolist(),
                "bottom_top_right": bottom_top_right.tolist()
            }
        }

        # Return the corner coordinates and detailed tag corners
        return {
            "top_right": top_right_int,
            "bottom_left": bottom_left_int,
            "top_top_left": top_top_left_int,
            "bottom_bottom_right": bottom_bottom_right_int
        }, detailed_positions
    else:
        print("Exactly two AR tags were not detected in the image.")
        return None, None


# Call the function
if __name__ == "__main__":
    # image_path = '../wall_image/eye_to_tag.png'
    image_path = '../wall_image/projector_to_tag.png'
    output_path = '../wall_image/tagged_image_correct.png'
    corners, detailed_positions = process_ar_tags(image_path, output_path)
    if corners:
        print(f"Top right corner: {corners['top_right']}")
        print(f"Bottom left corner: {corners['bottom_left']}")
        print(f"Top top left corner: {corners['top_top_left']}")
        print(f"Bottom bottom right corner: {corners['bottom_bottom_right']}")

    if detailed_positions:
        print("Detailed tag positions:")
        for tag, positions in detailed_positions.items():
            print(f"Tag {tag} detailed positions: {positions}")
