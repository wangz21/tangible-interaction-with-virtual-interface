# -*- coding: UTF-8 -*- #
"""
@fileName: combinedDetection.py
@author: Dextor
@time: 2024-07-15
"""

import sys
import os
import threading
import tkinter as tk
from PIL import Image, ImageTk
import numpy as np
import cv2
import time as tm  # Use 'tm' for time module
import queue
import mss
import pickle

sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
from DataProcessing.SceneTransform import process_and_display_transformation
from DataProcessing.inv_perspective import process_and_overlay_tags
from DataProcessing.ScreenProcessing import ScreenProcessor


class OnceWhiteDetectionApp:
    def __init__(self):
        self.detected_resolution = None
        self.is_running = False
        self.detect_enabled = False

    def display_cv_window(self):
        cv2.namedWindow('First Screen Detection')
        cv2.setMouseCallback('First Screen Detection', self.on_mouse)

        with mss.mss() as sct:
            monitor = sct.monitors[1]  # Use the first monitor

            while True:
                screen = sct.grab(monitor)
                screen_np = np.array(screen)
                screen_bgr = cv2.cvtColor(screen_np, cv2.COLOR_BGRA2BGR)

                # Draw a button on the image
                cv2.rectangle(screen_bgr, (10, 10), (110, 60), (0, 255, 0), -1)
                cv2.putText(screen_bgr, "Detect", (20, 40), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 255, 255), 2)

                cv2.imshow('First Screen Detection', screen_bgr)

                if cv2.waitKey(1) & 0xFF == ord('q'):
                    self.on_close()
                    break

                if self.detected_resolution:
                    break

        cv2.destroyAllWindows()

    def on_mouse(self, event, x, y, flags, param):
        if event == cv2.EVENT_LBUTTONDOWN:
            # Check if the click is within the button
            if 10 <= x <= 110 and 10 <= y <= 60:
                self.detect_enabled = True
                self.detect_white_area()

    def detect_white_area(self):
        if self.is_running:
            return
        self.is_running = True

        with mss.mss() as sct:
            monitor = sct.monitors[1]  # Use the first monitor
            screen = sct.grab(monitor)
            screen_np = np.array(screen)

            screen_bgr = cv2.cvtColor(screen_np, cv2.COLOR_BGRA2BGR)
            gray = cv2.cvtColor(screen_bgr, cv2.COLOR_BGR2GRAY)

            _, thresh = cv2.threshold(gray, 200, 255, cv2.THRESH_BINARY)
            contours, _ = cv2.findContours(thresh, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

            if contours:
                largest_contour = max(contours, key=cv2.contourArea)
                x, y, w, h = cv2.boundingRect(largest_contour)

                if w < 1280 or h < 720:
                    w, h = 1280, 720

                resolution = (w, h)
                cv2.rectangle(screen_bgr, (x, y), (x + w, y + h), (0, 255, 0), 2)
            else:
                resolution = (1280, 720)

            self.detected_resolution = resolution
            print(f"First screen detected resolution: {resolution}")
            cv2.destroyAllWindows()  # Only destroy OpenCV windows, don't exit the program

    def get_detected_resolution(self):
        self.display_cv_window()
        return self.detected_resolution


class WhiteAreaDetectionApp:
    def __init__(self, root, resolution_callback, result_queue, detection_interval=0.5):
        self.root = root
        self.root.title("White Area Detection")

        self.is_running = False
        self.resolution = (1280, 720)
        self.last_resolution = None  # Add this line
        self.queue = queue.Queue()
        self.result_queue = result_queue
        self.resolution_callback = resolution_callback  # Used to return the detected resolution
        self.stop_event = threading.Event()
        self.processing_thread = None
        self.thread_lock = threading.Lock()
        self.detection_interval = detection_interval  # Time interval between detections
        self.ready_for_detection = False  # Flag to control detection

        # Create the result window
        self.result_panel = tk.Label(self.root)
        self.result_panel.pack()

        self.screen_processor = None  # Add screen processor attribute

        self.root.protocol("WM_DELETE_WINDOW", self.on_close)

        self.update_result_window()
        self.check_files_and_prepare()

    def check_files_and_prepare(self):
        required_files = [
            '../data/coordinate/ar_tag_position.pkl',
            '../data/coordinate/ar_tag_all_points.pkl',
            '../data/coordinate/projection_area.pkl',
        ]
        if all(os.path.exists(file) for file in required_files):
            self.ready_for_detection = True
        else:
            self.ready_for_detection = False
            process_and_overlay_tags()
            if all(os.path.exists(file) for file in required_files):
                self.ready_for_detection = True

    def detect_white_areas(self):
        with mss.mss() as sct:
            # Get information of the second monitor
            monitor = sct.monitors[2]  # Use the second monitor
            while self.is_running:
                if not self.ready_for_detection:
                    continue

                start_time = tm.time()

                # Screenshot the second screen
                screen = sct.grab(monitor)
                screen_np = np.array(screen)

                # Convert the image from BGRA to BGR (OpenCV uses the BGR format)
                screen_bgr = cv2.cvtColor(screen_np, cv2.COLOR_BGRA2BGR)

                # Convert to grayscale
                gray = cv2.cvtColor(screen_bgr, cv2.COLOR_BGR2GRAY)

                # Use a high threshold to detect white areas
                _, thresh = cv2.threshold(gray, 200, 255, cv2.THRESH_BINARY)  # Adjust threshold value

                # Find contours
                contours, _ = cv2.findContours(thresh, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

                # Find the largest white area
                if contours:
                    largest_contour = max(contours, key=cv2.contourArea)
                    x, y, w, h = cv2.boundingRect(largest_contour)

                    # If the detected white area is smaller than 1280x720, return default value
                    if w < 1280 or h < 720:
                        w, h = 1280, 720

                    self.resolution = (w, h)

                    # Draw the rectangle on the original image
                    cv2.rectangle(screen_bgr, (x, y), (x + w, y + h), (0, 255, 0), 2)
                else:
                    # No white area found, return default value
                    self.resolution = (1280, 720)

                # Convert image back to RGB
                screen_rgb = cv2.cvtColor(screen_bgr, cv2.COLOR_BGR2RGB)
                screen_pil = Image.fromarray(screen_rgb)

                # Resize image
                max_size = (800, 600)
                screen_pil = screen_pil.resize(max_size, Image.LANCZOS)

                # Put image into the queue
                self.queue.put(screen_pil)

                # Call the callback function only if resolution has changed
                if self.resolution != self.last_resolution:  # Add this condition
                    self.last_resolution = self.resolution  # Update last resolution
                    self.start_processing_thread(self.resolution)  # Start processing thread

                # Sleep for the detection interval minus the time taken for processing
                time_to_sleep = self.detection_interval - (tm.time() - start_time)
                if time_to_sleep > 0:
                    tm.sleep(time_to_sleep)

    def start_processing_thread(self, resolution):
        with self.thread_lock:
            # if is used to terminate the processing thread.
            if self.processing_thread and self.processing_thread.is_alive():
                self.stop_event.set()
                self.processing_thread.join()

            # This part is used to start the new thread and make some setting
            self.stop_event.clear()
            self.processing_thread = threading.Thread(target=self.process_transformation, args=(resolution,))
            # Set the thread as a daemon thread, which means that the thread will automatically terminate
            # when the main program exits
            self.processing_thread.daemon = True
            self.processing_thread.start()

    def process_transformation(self, resolution):
        proj_image_path = '../wall_image/projector_to_tag.png'
        M_proj_inv, transformed_points = process_and_display_transformation(proj_image_path, resolution, 800)
        result = {
            "resolution": resolution,
            "transformed_points": transformed_points,
            "M_proj_inv": M_proj_inv  # Add M_proj to the result dictionary
        }
        self.resolution_callback(result)
        self.result_queue.put(result)

        # Start the screen processor with the new resolution
        self.start_screen_processor(self.resolution, resolution)

    def start_screen_processor(self, first_resolution, latest_resolution):
        if self.screen_processor:
            print("Screen processor exists, updating resolution...")
            self.screen_processor.update_resolution(first_resolution, latest_resolution)
        else:
            print("Creating new screen processor...")
            self.screen_processor = ScreenProcessor(first_resolution, latest_resolution, display_scale=1.0)
            self.screen_processor_thread = threading.Thread(target=self.screen_processor.capture_screen)
            self.screen_processor_thread.start()
            print("Screen processor created and started")

    def update_result_window(self):
        try:
            while not self.queue.empty():
                screen_pil = self.queue.get_nowait()
                img = ImageTk.PhotoImage(screen_pil)
                self.result_panel.configure(image=img)
                self.result_panel.image = img  # Keep a reference to avoid garbage collection
        except queue.Empty:
            pass
        self.root.after(100, self.update_result_window)

    def start_detection(self):
        if not self.is_running:
            self.is_running = True
            self.detection_thread = threading.Thread(target=self.detect_white_areas)
            self.detection_thread.daemon = True
            self.detection_thread.start()

    def stop_detection(self):
        self.is_running = False
        if hasattr(self, 'detection_thread'):
            self.detection_thread.join()
        if hasattr(self, 'processing_thread') and self.processing_thread.is_alive():
            self.stop_event.set()
            self.processing_thread.join()

    def on_close(self):
        self.stop_detection()
        self.root.destroy()
        sys.exit()  # Ensure the entire application exits when the window is closed


def get_white_area_resolution(resolution_callback):
    result_queue = queue.Queue()  # Queue to store detected resolutions

    # Run OnceWhiteDetectionApp to get the first resolution
    once_detection = OnceWhiteDetectionApp()
    first_resolution = once_detection.get_detected_resolution()

    # Set up the Tkinter root window and the WhiteAreaDetectionApp
    root = tk.Tk()
    app = WhiteAreaDetectionApp(root, resolution_callback, result_queue, detection_interval=0.5)

    # Set the initial resolution for the screen processor
    app.start_screen_processor(first_resolution, first_resolution)

    def check_queue():
        root.after(100, check_queue)

    root.after(100, check_queue)

    # Start detection
    app.start_detection()

    root.mainloop()


if __name__ == "__main__":
    def resolution_callback(result):
        print(f"Detected resolution: {result['resolution']}")
        # print(f"Transformed points: {result['transformed_points']}")
        # print(f"Perspective Transformation Matrix: {result['M_proj_inv']}")


    get_white_area_resolution(resolution_callback)
