# -*- coding: UTF-8 -*- #
"""
@fileName:tagDetection.py
@author:Dextor
@time:2024-07-05
"""

import cv2
import numpy as np
from cv2 import aruco
import matplotlib
import matplotlib.pyplot as plt
import time
import os
import pickle
from scipy.spatial.transform import Rotation as R

matplotlib.use('TkAgg')

# Flag to indicate when to start capturing
start_capture = False

# Capture steps
steps = ['projector_to_tag', 'eye_to_tag']
step_tags = [['id_60', 'id_80'], ['id_60', 'id_80']]  # Corresponding tags for each step

# Data storage
captured_data = {}
all_points = {}

# Create the output directory if it doesn't exist
output_dir = "../wall_image"
os.makedirs(output_dir, exist_ok=True)

param_file_path = '../data/coordinate/aruco_params.pkl'  # 参数文件路径


# Read the parameters from the file
def load_parameters(file_path):
    with open(file_path, 'rb') as file:
        parameters = pickle.load(file)
    return parameters


# Apply the parameter to the ArUco detector
def apply_parameters(parameters):
    params = aruco.DetectorParameters()
    params.adaptiveThreshWinSizeMin = parameters['adaptiveThreshWinSizeMin']
    params.adaptiveThreshWinSizeMax = parameters['adaptiveThreshWinSizeMax']
    params.adaptiveThreshWinSizeStep = parameters['adaptiveThreshWinSizeStep']
    params.minMarkerPerimeterRate = parameters['minMarkerPerimeterRate']
    params.maxMarkerPerimeterRate = parameters['maxMarkerPerimeterRate']
    params.adaptiveThreshConstant = parameters['adaptiveThreshConstant']
    params.minCornerDistanceRate = parameters['minCornerDistanceRate']
    params.minDistanceToBorder = parameters['minDistanceToBorder']
    params.minMarkerDistanceRate = parameters['minMarkerDistanceRate']
    params.cornerRefinementMethod = parameters['cornerRefinementMethod']
    params.cornerRefinementWinSize = parameters['cornerRefinementWinSize']
    params.cornerRefinementMaxIterations = parameters['cornerRefinementMaxIterations']
    params.cornerRefinementMinAccuracy = parameters['cornerRefinementMinAccuracy']
    params.markerBorderBits = parameters['markerBorderBits']
    params.perspectiveRemovePixelPerCell = parameters['perspectiveRemovePixelPerCell']
    params.perspectiveRemoveIgnoredMarginPerCell = parameters['perspectiveRemoveIgnoredMarginPerCell']
    params.maxErroneousBitsInBorderRate = parameters['maxErroneousBitsInBorderRate']
    params.minOtsuStdDev = parameters['minOtsuStdDev']
    params.errorCorrectionRate = parameters['errorCorrectionRate']
    return params


def on_mouse(event, x, y, flags, param):
    global start_capture
    if event == cv2.EVENT_LBUTTONDOWN:
        if 10 <= x <= 110 and 10 <= y <= 60:  # Check if the click is within the button area
            start_capture = True


def detect_ar_tag_position(camera_index=0, duration=5):
    global start_capture, captured_data, all_points

    cap = cv2.VideoCapture(camera_index)

    # Check if the camera is opened
    if not cap.isOpened():
        print("Can't open the camera")
        return None, None

    # Load the ArUco dictionary and detector parameters
    aruco_dict = aruco.getPredefinedDictionary(aruco.DICT_ARUCO_ORIGINAL)
    parameters = load_parameters(param_file_path)  # Read the parameters from the document
    detector_params = apply_parameters(parameters)  # Apply the parameters to the detector

    # Camera calibration parameters (these need to be determined through camera calibration)
    camera_matrix = np.array([[800, 0, 320],
                              [0, 800, 240],
                              [0, 0, 1]], dtype=np.float32)
    dist_coeffs = np.zeros((5, 1))

    current_step = 0  # Ensure current_step starts from 0 every time

    while current_step < len(steps):
        start_capture = False
        world_coordinates_dict = {tag: [] for tag in step_tags[current_step]}
        rvecs_dict = {tag: [] for tag in step_tags[current_step]}
        tvecs_dict = {tag: [] for tag in step_tags[current_step]}
        last_saved_frames = []  # Initialize for both steps

        # Create a window and set a mouse callback to capture button clicks
        cv2.namedWindow('Camera View')
        cv2.setMouseCallback('Camera View', on_mouse)

        while not start_capture:
            # Capture the frame
            ret, frame = cap.read()

            # Check if read the frame correctly
            if not ret:
                break

            # Draw the button and instructions on the frame
            cv2.rectangle(frame, (10, 10), (110, 60), (0, 255, 0), -1)
            cv2.putText(frame, 'Start', (20, 40), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 255, 255), 2)
            cv2.putText(frame, f"Step: {steps[current_step]}", (10, 100), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 255, 255), 2)

            # Display the frame
            cv2.imshow('Camera View', frame)

            # Check for 'q' key to exit
            if cv2.waitKey(1) & 0xFF == ord('q'):
                cap.release()
                cv2.destroyAllWindows()
                return None, None

        # Start time
        start_time = time.time()

        while time.time() - start_time < duration:  # Run for approximately `duration` seconds
            # Capture the frame
            ret, frame = cap.read()

            # Check if read the frame correctly
            if not ret:
                break

            # Store the unprocessed frame
            unprocessed_frame = frame.copy()

            # Preprocess the frame
            gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
            gray = cv2.GaussianBlur(gray, (5, 5), 0)

            # Detect the ArUco tag
            corners, ids, rejected = aruco.detectMarkers(gray, aruco_dict, parameters=detector_params)

            # If detect the tag
            if ids is not None:
                # Draw the frame of the tag
                frame = aruco.drawDetectedMarkers(frame, corners, ids)

                # Assuming each AR Tag's actual size (e.g., in meters)
                marker_length = 0.04  # AR Tag's side length is 0.04 meters (4 cm)

                # Estimate pose of each marker
                rvecs, tvecs, _ = aruco.estimatePoseSingleMarkers(corners, marker_length, camera_matrix, dist_coeffs)

                for rvec, tvec, id in zip(rvecs, tvecs, ids):
                    # Check if the ID is in the list
                    tag_id = f'id_{id[0]}'
                    if tag_id in world_coordinates_dict:
                        # Compute world coordinates
                        R_matrix, _ = cv2.Rodrigues(rvec)
                        z_axis = R_matrix[:, 2]

                        # Check if z-axis is pointing towards the camera
                        if z_axis[2] < 0:  # z axis point to the camera
                            # Draw axis for the marker
                            cv2.drawFrameAxes(frame, camera_matrix, dist_coeffs, rvec, tvec, marker_length)

                            # Store world coordinates based on the ID
                            world_coordinates = np.dot(R_matrix, np.array([0, 0, 0])) + tvec.reshape((3,))
                            world_coordinates_dict[tag_id].append(world_coordinates)
                            rvecs_dict[tag_id].append(rvec)
                            tvecs_dict[tag_id].append(tvec)

            # Display the result frame
            cv2.imshow('Camera View', frame)

            # Save frames in the last 20 frames
            if len(last_saved_frames) >= 10:
                last_saved_frames.pop(0)  # Remove the oldest frame if we already have 20
            last_saved_frames.append(unprocessed_frame)

            # Press 'q' to exit early
            if cv2.waitKey(1) & 0xFF == ord('q'):
                break

        # Save the last 10 captured frames with required tags for both steps
        for i, saved_frame in enumerate(last_saved_frames):
            frame_filename = os.path.join(output_dir, f'{steps[current_step]}_frame_{i + 1}.png')
            cv2.imwrite(frame_filename, saved_frame)

        # Store all points for the current step
        all_points[steps[current_step]] = world_coordinates_dict

        # Remove outliers and calculate the average position
        def remove_outliers_iqr(coordinates_list):
            if len(coordinates_list) > 0:
                coordinates = np.array(coordinates_list)
                q1 = np.percentile(coordinates, 25, axis=0)
                q3 = np.percentile(coordinates, 75, axis=0)
                iqr = q3 - q1
                lower_bound = q1 - 0.5 * iqr
                upper_bound = q3 + 0.5 * iqr
                mask = (coordinates >= lower_bound) & (coordinates <= upper_bound)
                return coordinates[np.all(mask, axis=-1)]
            return []

        def calculate_average_position(coordinates_list):
            if len(coordinates_list) > 0:
                return np.mean(coordinates_list, axis=0)
            else:
                return np.array([0, 0, 0])

        # Process data for each ID separately
        average_positions = {}
        rvecs_avg = {}
        tvecs_avg = {}
        for key, points in world_coordinates_dict.items():
            filtered_coordinates = remove_outliers_iqr(points)
            average_position = calculate_average_position(filtered_coordinates)
            average_positions[key] = average_position

            # Calculate average rvec and tvec
            filtered_rvecs = remove_outliers_iqr(rvecs_dict[key])
            filtered_tvecs = remove_outliers_iqr(tvecs_dict[key])
            if len(filtered_rvecs) > 0:
                # Convert rotation vectors to quaternions
                rotations = R.from_rotvec(filtered_rvecs)
                avg_rotation = rotations.mean()
                rvecs_avg[key] = avg_rotation.as_rotvec()
            else:
                rvecs_avg[key] = np.array([0, 0, 0])

            tvecs_avg[key] = calculate_average_position(filtered_tvecs)

        # Store the average positions, rvecs, and tvecs for the current step
        captured_data[steps[current_step]] = {
            'average_positions': average_positions,
            'rvecs_avg': rvecs_avg,
            'tvecs_avg': tvecs_avg
        }

        # Proceed to the next step
        current_step += 1

    # Release the camera and close the window
    cap.release()
    cv2.destroyAllWindows()

    return captured_data, all_points


def plot_all_points(all_points, duration):
    for step, points_dict in all_points.items():
        for tag_id, points in points_dict.items():
            if len(points) > 0:
                coordinates = np.array(points)
                distances = np.linalg.norm(coordinates, axis=1)
                times = np.linspace(0, duration, len(coordinates))  # Generate time values for the plot
                plt.figure()
                plt.plot(times, distances, marker='o')
                plt.xlabel('Time (s)')
                plt.ylabel('Distance to AR Tag (m)')
                plt.title(f'Distance to AR Tag Over Time for {step} - {tag_id}')
                plt.show()
            else:
                print(f"No world coordinates to plot for {step} - {tag_id}.")


if __name__ == "__main__":
    captured_data, all_points = detect_ar_tag_position(camera_index=0, duration=5)

    # Plot all points for each step
    plot_all_points(all_points, duration=5)
